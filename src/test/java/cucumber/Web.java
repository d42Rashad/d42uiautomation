package cucumber;

import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.TrustStrategy;
import org.apache.http.util.EntityUtils;
import org.testng.Assert;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class Web {

    public static void postAdaptavistResults(Adaptavist.AdaptavistPayload apl) throws IOException {
        if(Boolean.parseBoolean(Config.get("postToAdaptavist"))) {
            String postUrl = Config.get("adaptavist_url");
            Gson gson = new Gson();
            HttpClient httpClient = HttpClientBuilder.create().build();
            HttpPost post = new HttpPost(postUrl);
            StringEntity postingString = new StringEntity(gson.toJson(apl));
            post.setEntity(postingString);
            post.setHeader("Content-type", "application/json");
            post.setHeader("Authorization", "Bearer " + Config.get("adaptavist_bearer_token"));
            HttpResponse response = httpClient.execute(post);
            System.out.println(response.getStatusLine());
        }
    }

    public static String getViewJobScoreV1Performance(String view, String jobName) throws IOException, InterruptedException {

        String postUrl = Config.get("host") + "/services/data/v1.0/query/";

//        Gson gson = new Gson();
//        HttpClient httpClient = HttpClientBuilder.create().build();

//        System.out.println("view: " + view);
//        System.out.println("jobName: " + jobName);

        HttpPost post = new HttpPost(postUrl);

        String query = ResourceBundle.getBundle(view).getString("query").replace("$",jobName);

//        System.out.println("query: " + query);

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("query", query));
        post.setEntity(new UrlEncodedFormEntity(params));

        post.setHeader("Accept", "application/json");
        post.setHeader("Content-type", "application/x-www-form-urlencoded");
        post.setHeader("Authorization", "Basic " + "YWRtaW46YWRtIW5kNDI=");

        CloseableHttpClient chp = getCloseableHttpClient();
        CloseableHttpResponse res = chp.execute(post);

        if (res.getStatusLine().getStatusCode() != 200) {
            getViewJobScoreV1Performance(view, jobName);
            Thread.sleep(10000);
        }

        Assert.assertEquals(res.getStatusLine().getStatusCode(), 200, "Validating status code of insert.");
//        System.out.println("--->>" + res.getEntity());
        return EntityUtils.toString(res.getEntity());

    }

    public static CloseableHttpClient getCloseableHttpClient() {
        CloseableHttpClient httpClient = null;
        try {
            httpClient = HttpClients.custom().
                    setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE).
                    setSSLContext(new SSLContextBuilder().loadTrustMaterial(null, new TrustStrategy()
                    {
                        public boolean isTrusted(X509Certificate[] arg0, String arg1) throws CertificateException
                        {
                            return true;
                        }
                    }).build()).build();
        } catch (KeyManagementException e) {
            System.out.println("KeyManagementException in creating http client instance: " + e);
        } catch (NoSuchAlgorithmException e) {
            System.out.println("NoSuchAlgorithmException in creating http client instance" + e);
        } catch (KeyStoreException e) {
            System.out.println("KeyStoreException in creating http client instance" + e);
        }
        return httpClient;
    }

}
