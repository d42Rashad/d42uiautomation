package cucumber;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;

public class DriverFactory {

    private DriverFactory() {
        //Do-nothing..Do not allow to initialize this class from outside
    }
    private static DriverFactory instance = new DriverFactory();

    public static DriverFactory getInstance() {
        return instance;
    }

    ThreadLocal<WebDriver> driver = new ThreadLocal<WebDriver>() {
        @Override
        protected WebDriver initialValue() {
            String browser = Config.get("browser");
            String browser_version = Config.get("browser_version");
            String grid_url = checkGrid(0);
            switch(browser.trim()) {
                case "CHROME":
                    ChromeOptions chrome_options = new ChromeOptions();
                    chrome_options.setCapability("version", browser_version);
                    chrome_options.setCapability("platform", Platform.LINUX);
                    chrome_options.setCapability (CapabilityType.ACCEPT_SSL_CERTS, true);
                    chrome_options.setCapability (CapabilityType.ACCEPT_INSECURE_CERTS, true);
                    try {
                        return new RemoteWebDriver(new URL(grid_url),chrome_options);
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                    break;
                case "FIREFOX":
                    FirefoxOptions ff_options = new FirefoxOptions();
                    ff_options.setCapability("version", browser_version);
                    ff_options.setCapability("platform", Platform.LINUX);
                    ff_options.setCapability (CapabilityType.ACCEPT_SSL_CERTS, true);
                    ff_options.setCapability (CapabilityType.ACCEPT_INSECURE_CERTS, true);
                    ff_options.setCapability (CapabilityType.UNHANDLED_PROMPT_BEHAVIOUR, "accept");
                    ff_options.setCapability (CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, "accept");
                    ff_options.setCapability("marionette", true);
                    try {
                        return new RemoteWebDriver(new URL(grid_url),ff_options);
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                    break;
            }
            driver.get().manage().deleteAllCookies();
            return null;
        }
    };

    public WebDriver getDriver() {
        return driver.get();
    }

    public void removeDriver() {
        driver.get().quit();
        driver.remove();
    }

    private String checkGrid(int retry) {
        String grid_ip = Config.get("grid_ip");
        int grid_port = Integer.parseInt(Config.get("grid_port"));
        int ping_timeout = Integer.parseInt(Config.get("ping_timeout"));
        String grid_url = "http://" + grid_ip + ":" + grid_port + "/wd/hub";

        if(!pingHost(grid_ip,grid_port,ping_timeout) && retry <= 6) {
            Wait.med();
            checkGrid(retry + 1);
        }

        Boolean can_ping = pingHost(grid_ip,grid_port,ping_timeout);
        Assert.assertTrue(can_ping, "CANNOT CONNECT TO GRID AT " + grid_url);
        return grid_url;
    }

    public boolean pingHost(String host, int port, int timeout) {
        try (Socket socket = new Socket()) {
            socket.connect(new InetSocketAddress(host, port), timeout);
            return true;
        } catch (IOException e) {
            return false;
        }
    }
}