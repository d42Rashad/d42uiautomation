package cucumber.stepdefinitions;

import cucumber.BasePage;
import cucumber.BaseStep;
import cucumber.Table;
import io.cucumber.java.en.Then;

import java.io.IOException;

public class Room implements BaseStep {

    @Override @Then("Room - type - obj: {string} text: {string} isVariable: {string}")
    public void type(String obj, String text, String isVariable) {
        BasePage.type(cucumber.pages.Room.getElement(obj), text, isVariable, cucumber.pages.Room.variableMap);
    }

    @Override @Then("Room - click - obj: {string}")
    public void click(String obj) {
        BasePage.click(cucumber.pages.Room.getElement(obj));
    }

    @Override
    public void is_displayed(String obj) {

    }

    @Override @Then("Room - select - obj: {string} value: {string}")
    public void select(String obj, String text) {
        BasePage.select(cucumber.pages.Room.getElement(obj), text);
    }

    @Override
    public void check_attribute(String obj, String attribute, String value) {

    }

    @Override @Then("Room - check-search-table-value - obj: {string} column: {string} row: {string} value: {string} variable: {string}")
    public void check_search_table_value(String obj, String column, String row, String value, String isVariable) {
        BasePage.check_search_table_value(Table.parse(cucumber.pages.Room.getElement(obj)),column, row, value, isVariable,cucumber.pages.Room.variableMap);
    }

    @Override @Then("Room - click-search-table-value - obj: {string} column: {string} row: {string}")
    public void click_search_table_value(String obj, String column, String row) {
        BasePage.click_search_table_value(Table.parse(cucumber.pages.Room.getElement(obj)), row, column);
    }

    @Override @Then("Room - validate-text - obj: {string} value: {string}")
    public void validate_text(String obj, String value) {
        BasePage.assertEquals(cucumber.pages.Room.getElement(obj).getText(), value);
    }

    @Override
    public void validate_table_column_header(String obj, String columnIndex, String value) {

    }

    @Override  @Then("Room - set-variable - key: {string} value: {string} timestamp: {string} random: {string}")
    public void set_variable(String key, String val, String timestamp, String random) {
        BasePage.set_variable(cucumber.pages.Room.variableMap, key, val, timestamp, random);
    }

    @Override
    public void hover(String obj) {

    }

    @Override
    public void validate_title(String text) {

    }

    @Override
    public void select_table_value(String obj, String column, String row, String value, String isVariable) {

    }

    @Override  @Then("Room - validate-table-value - obj: {string} column: {string} row: {string} value: {string}")
    public void validate_table_value(String obj, String column, String row, String value) {
        BasePage.validate_table_value(Table.parseDynamicTable(cucumber.pages.Room.getElement(obj), Integer.parseInt(row)-1 , "p", column), value);
    }

    @Override
    public void clear(String obj) {

    }

    @Override
    public void validate_table_count(String obj, String text) {

    }

    @Override
    public void validate_table_column_value(String obj, String column, String value) {

    }

    @Override
    public void click_field_row_text(String obj, String column, String value) {

    }

    @Override @Then("Room - click-options - obj: {string} value: {string}")
    public void click_options(String obj, String value) {
        BasePage.click_options(cucumber.pages.Room.getElement(obj), value);
    }

    @Override @Then("Room - gridLayout-actions - action: {string} value: {string}")
    public void gridLayout_actions(String action, String value) {
        BasePage.select_action(BasePage.findWebElement(cucumber.pages.Room.roomLayoutGridText(value)), action);
    }

    @Override @Then("Room - validate-room-layout-property - property: {string} text: {string}")
    public void validate_room_layout_property(String property, String text) throws IOException {
        BasePage.validate_parsed_text(cucumber.pages.Room.layoutPropertyText(),":",property,text);
    }

    @Override
    public void validate_text_contains(String obj, String text) {

    }

    @Override
    public void validate_all_table_column_values(String obj, String column, String value) {

    }

    @Override
    public void click_results_table_value_by_key(String obj, String searchColumn, String clickColumn, String value) {

    }
}



















