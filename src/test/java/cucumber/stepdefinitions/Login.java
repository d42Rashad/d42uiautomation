package cucumber.stepdefinitions;

import cucumber.BasePage;
import cucumber.BaseStep;
import cucumber.Config;
import cucumber.Table;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

import java.io.IOException;

public class Login implements BaseStep {

    private String path = Config.env("login_path");
    private String homePageTitle = "Log in | Device42";

    @Given("Login - Navigate")
    public void navigate() {
        cucumber.pages.Login.navigate(path);
    }

    @Given("Logged in as admin")
    public void login_as_admin() {
        if(!BasePage.driver().getTitle().equals(homePageTitle)) {
            navigate();
            String username = Config.get("admin_username");
            String password = Config.get("admin_password");
            cucumber.pages.Login.loginAsAdmin(username, password);
        }
    }

    //----------------------SHARED BEGIN

    @Override @Then("Login - type - obj: {string} text: {string} isVariable: {string}")
    public void type(String obj, String text, String isVariable) {
        BasePage.type(cucumber.pages.Login.getElement(obj), text, isVariable, cucumber.pages.Login.variableMap);
    }

    @Override @Then("Login - click - obj: {string}")
    public void click(String obj) {
        BasePage.click(cucumber.pages.Login.getElement(obj));
    }

    @Override @Then("Login - isDisplayed - obj: {string}")
    public void is_displayed(String obj) {
        BasePage.is_displayed(cucumber.pages.Login.getElement(obj));
    }

    @Override @Then("Login - Select {string} from {string}")
    public void select(String obj, String text) {
        BasePage.select(cucumber.pages.Login.getElement(obj), text);
    }

    @Override @Then("Login - {string} attribute {string} is {string}")
    public void check_attribute(String obj, String attribute, String value) {
        BasePage.check_attribute(cucumber.pages.Login.getElement(obj),attribute,value);
    }

    @Override @Then("Login - Table {string} - column: {string} row: {string} click")
    public void click_search_table_value(String obj, String column, String row) {
        BasePage.click_search_table_value(Table.parse(cucumber.pages.Login.getElement(obj)), row, column);
    }

    @Override @Then("Login - Text for {string} is {string}")
    public void validate_text(String obj, String value) {
        BasePage.assertEquals(cucumber.pages.Login.getElement(obj).getText(), value);
    }

    @Override @Then("Login - Table {string} - column-header-index: {string} value: {string}")
    public void validate_table_column_header(String obj, String columnIndex, String value) {
        BasePage.validate_column_header(cucumber.pages.Login.getElement(obj),columnIndex,value);
    }

    @Override @Then("Login - Hover {string}")
    public void hover(String obj) {
        BasePage.hover(cucumber.pages.Login.getElement(obj));
    }

    @Override @Then("Login - Title is {string}")
    public void validate_title(String text) {
        BasePage.validate_title(text);
    }

    @Override
    public void select_table_value(String obj, String column, String row, String value, String isVariable) {

    }

    @Override
    public void clear(String obj) {

    }

    @Override
    public void validate_table_count(String obj, String text) {

    }

    @Override
    public void validate_table_column_value(String obj, String column, String value) {

    }

    @Override
    public void click_field_row_text(String obj, String column, String value) {

    }

    @Override
    public void click_options(String obj, String value) {

    }

    @Override
    public void gridLayout_actions(String action, String value) {

    }

    @Override
    public void validate_room_layout_property(String property, String text) throws IOException {

    }

    @Override
    public void validate_text_contains(String obj, String text) {

    }

    @Override
    public void validate_all_table_column_values(String obj, String column, String value) {

    }

    @Override
    public void click_results_table_value_by_key(String obj, String searchColumn, String clickColumn, String value) {

    }

    @Override @Then("Login - Set Variable-  key: {string} value: {string} timestamp: {string}")
    public void set_variable(String key, String val, String timestamp, String random) {
        BasePage.set_variable(cucumber.pages.Login.variableMap, key, val, timestamp, random);
    }

    @Override
    public void validate_table_value(String obj, String column, String row, String value) {

    }

    @Override @Then("Login - Table {string} - column: {string} row: {string} value: {string} variable: {string}")
    public void check_search_table_value(String obj, String column, String row, String value, String isVariable) {
        BasePage.check_search_table_value(Table.parse(cucumber.pages.Login.getElement(obj)),column, row, value, isVariable,cucumber.pages.Login.variableMap);
    }

}
