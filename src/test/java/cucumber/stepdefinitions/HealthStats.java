package cucumber.stepdefinitions;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import cucumber.Config;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.testng.Assert;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class HealthStats {

    @Given("HealthStats - status-check")
    public void status_check() {
        Assert.assertEquals(getCode(Config.get("host")),"200");
    }

    public String getCode(String url) {
        int code = 200;
        try {
            URL siteURL = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) siteURL.openConnection();
            connection.setRequestMethod("GET");
            connection.setConnectTimeout(3000);
            connection.connect();
            code = connection.getResponseCode();
        } catch (Exception e) {
        }
        return String.valueOf(code);
    }


    @Then("HealthStats - validate-attribute - attribute: {string} value: {string}")
    public void validate_attribute(String attribute, String value) {
        String json = get(Config.get("host") + ":4343/healthstats");
        Gson gson = new Gson();
        healthStatusResponseClass hrsc = gson.fromJson(json, healthStatusResponseClass.class);
        switch(attribute) {
            case "version":
                Assert.assertEquals(hrsc.version, Config.get(attribute), "Checking the version of the build.");
                break;
        }
    }

    @Then("HealthStats - validate-rc-count - value: {string}")
    public void validate_rc_count(String value) {
        String json = get(Config.get("host") + ":4343/healthstats");
        Gson gson = new Gson();
        healthStatusResponseClass hrsc = gson.fromJson(json, healthStatusResponseClass.class);
        Assert.assertEquals(hrsc.rc_stats.size(), Integer.parseInt(value), "Validating the count of the RC.");
    }

    @Then("HealthStats - validate-rc-status - value: {string} timeout: {string} retries: {string}")
    public void validate_rc_status(String value, String timeout, String retires) throws InterruptedException {
        String json = get(Config.get("host") + ":4343/healthstats");
        System.out.println("--->>>>" + json);
        Gson gson = new Gson();
        healthStatusResponseClass hrsc = gson.fromJson(json, healthStatusResponseClass.class);
        Boolean connected = false;
        for( rc r :hrsc.rc_stats) {
            System.out.println("-> r.rc_name: " + r.rc_name.trim());
            System.out.println("-> r.state: " + r.state.trim());
            if(r.rc_name.trim().toUpperCase().equals(value.toUpperCase()) && r.state.trim().equals("connected")) {
                connected = true;
            }
        }
        if(Integer.parseInt(retires) > 0 && !connected) {
            Thread.sleep(Integer.parseInt(timeout) * 1000);
            validate_rc_status(value, timeout, String.valueOf(Integer.parseInt(retires) - 1));
        }
        Assert.assertTrue(connected, "Validating if the RC is connected. (API)");
    }

    @Then("HealthStats - validate-rc-status - timeout: {string} retries: {string}")
    public void validate_rc_status(String timeout, String retires) throws InterruptedException {
        String json = get(Config.get("host") + ":4343/healthstats");
        Gson gson = new Gson();
        healthStatusResponseClass hrsc = gson.fromJson(json, healthStatusResponseClass.class);
        String allConnected = "connected";
        int counter = 0;
        for(rc r : hrsc.rc_stats) {
            if(!r.state.equals("connected")) {
                allConnected = "disconnected";
                counter++;
            }
        }
        if(Integer.parseInt(retires) > 0 && allConnected.equals("disconnected")) {
            Thread.sleep(Integer.parseInt(timeout) * 1000);
            validate_rc_status(timeout, String.valueOf(Integer.parseInt(retires) - 1));
        }
        Assert.assertEquals(allConnected + " (" + counter + ")", "connected (0)", "Checking RC status.");
    }

    public class healthStatusResponseClass {
        public String version;
        public String dbsize;
        public String cpu_used_percent ;
        public List<rc> rc_stats = new ArrayList<>();
        public List<bc> backup_status = new ArrayList<>();
        public String disk_used_percent = "";
        public memoryInMB memory_in_MB;
    }

    public class bc {

    }

    public class rc {
        public String state;
        public String rc_name;
        public status status;

    }

    public class status {
        public String memory;
        public String storage;
        public String la;
    }

    public class memoryInMB {
        public int    memtotal;
        public int    cached;
        public int    swapfree;
        public int    swaptotal;
        public int    memfree;
        public int    buffers;
    }

    public String get (String url) {
        HttpRequest request = HttpRequest.get(url);
        request.trustAllCerts();
        request.trustAllHosts();
        String response = request.body();
        return response;
    }
}
