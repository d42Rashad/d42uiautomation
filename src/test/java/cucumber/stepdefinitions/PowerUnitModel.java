package cucumber.stepdefinitions;

import cucumber.BasePage;
import cucumber.BaseStep;
import cucumber.Table;
import io.cucumber.java.en.Then;

import java.io.IOException;

public class PowerUnitModel implements BaseStep  {

    @Override @Then("PowerUnitModel - type - obj: {string} text: {string} isVariable: {string}")
    public void type(String obj, String text, String isVariable) {
        BasePage.type(cucumber.pages.PowerUnitModel.getElement(obj), text, isVariable, cucumber.pages.PowerUnitModel.variableMap);
    }

    @Override @Then("PowerUnitModel - click - obj: {string}")
    public void click(String obj) {
        BasePage.click(cucumber.pages.PowerUnitModel.getElement(obj));
    }

    @Override @Then("PowerUnitModel - isDisplayed - obj: {string}")
    public void is_displayed(String obj) {
        BasePage.is_displayed(cucumber.pages.PowerUnitModel.getElement(obj));
    }

    @Override @Then("PowerUnitModel - select - obj: {string} value: {string}")
    public void select(String obj, String text) {
        BasePage.select(cucumber.pages.PowerUnitModel.getElement(obj), text);
    }

    @Override
    public void check_attribute(String obj, String attribute, String value) {

    }

    @Override @Then("PowerUnitModel - check-search-table-value - obj: {string} column: {string} row: {string} value: {string} variable: {string}")
    public void check_search_table_value(String obj, String column, String row, String value, String isVariable) {
        BasePage.check_search_table_value(Table.parse(cucumber.pages.PowerUnitModel.getElement(obj)),column, row, value, isVariable,cucumber.pages.PowerUnitModel.variableMap);
    }

    @Override
    public void click_search_table_value(String obj, String column, String row) {

    }

    @Override @Then("PowerUnitModel - validate-text - obj: {string} value: {string}")
    public void validate_text(String obj, String value) {
        BasePage.assertEquals(cucumber.pages.PowerUnitModel.getElement(obj).getText(), value);
    }

    @Override
    public void validate_table_column_header(String obj, String columnIndex, String value) {

    }

    @Override  @Then("PowerUnitModel - set-variable - key: {string} value: {string} timestamp: {string} random: {string}")
    public void set_variable(String key, String val, String timestamp, String random) {
        BasePage.set_variable(cucumber.pages.PowerUnitModel.variableMap, key, val, timestamp, random);
    }

    @Override
    public void hover(String obj) {

    }

    @Override @Then("PowerUnitModel - validate-title - text: {string}")
    public void validate_title(String text) {
        BasePage.assertEquals(cucumber.pages.PowerUnitModel.driver().getTitle(), text);
    }

    @Override
    public void select_table_value(String obj, String column, String row, String value, String isVariable) {

    }

    @Override
    public void validate_table_value(String obj, String column, String row, String value) {

    }

    @Override
    public void clear(String obj) {

    }

    @Override
    public void validate_table_count(String obj, String text) {

    }

    @Override
    public void validate_table_column_value(String obj, String column, String value) {

    }

    @Override
    public void click_field_row_text(String obj, String column, String value) {

    }

    @Override
    public void click_options(String obj, String value) {

    }

    @Override
    public void gridLayout_actions(String action, String value) {

    }

    @Override
    public void validate_room_layout_property(String property, String text) throws IOException {

    }

    @Override
    public void validate_text_contains(String obj, String text) {

    }

    @Override
    public void validate_all_table_column_values(String obj, String column, String value) {

    }

    @Override
    public void click_results_table_value_by_key(String obj, String searchColumn, String clickColumn, String value) {

    }

}
