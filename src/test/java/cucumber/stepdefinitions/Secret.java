package cucumber.stepdefinitions;

import cucumber.BasePage;
import cucumber.BaseStep;
import cucumber.Table;
import io.cucumber.java.en.Then;

import java.io.IOException;

public class Secret implements BaseStep {

    @Override @Then("Secret - type - obj: {string} text: {string} isVariable: {string}")
    public void type(String obj, String text, String isVariable) {
        BasePage.type(cucumber.pages.Secret.getElement(obj), text, isVariable, cucumber.pages.Secret.variableMap);
    }

    @Override @Then("Secret - click - obj: {string}")
    public void click(String obj) {
        BasePage.click(cucumber.pages.Secret.getElement(obj));
    }

    @Override @Then("Secret - isDisplayed - obj: {string}")
    public void is_displayed(String obj) {
        BasePage.is_displayed(cucumber.pages.Secret.getElement(obj));
    }

    @Override @Then("Secret - select - obj: {string} value: {string}")
    public void select(String obj, String text) {
        BasePage.select(cucumber.pages.Secret.getElement(obj), text);
    }

    @Override @Then("Secret - check-attribute - obj: {string} attribute: {string} value: {string}")
    public void check_attribute(String obj, String attribute, String value) {
        BasePage.check_attribute(cucumber.pages.Secret.getElement(obj),attribute,value);
    }

    @Override @Then("Secret - click-search-table-value - obj: {string} column: {string} row: {string}")
    public void click_search_table_value(String obj, String column, String row) {
        BasePage.click_search_table_value(Table.parse(cucumber.pages.Secret.getElement(obj)), row, column);
    }

    @Override @Then("Secret - validate-text - obj: {string} value: {string}")
    public void validate_text(String obj, String value) {
        BasePage.assertEquals(cucumber.pages.Secret.getElement(obj).getText(), value);
    }

    @Override @Then("Secret - validate-table-column-header - obj: {string} index: {string} value: {string}")
    public void validate_table_column_header(String obj, String columnIndex, String value) {
        BasePage.validate_column_header(cucumber.pages.Secret.getElement(obj),columnIndex,value);
    }

    @Override @Then("Secret - hover - obj: {string}")
    public void hover(String obj) {
        BasePage.hover(cucumber.pages.Secret.getElement(obj));
    }

    @Override @Then("Secret - validate-title - text: {string}")
    public void validate_title(String text) {
        BasePage.validate_title(text);
    }

    @Override @Then("Secret - select-table-value - obj: {string} column: {string} row: {string} value: {string} variable: {string}")
    public void select_table_value(String obj, String column, String row, String value, String isVariable) {

    }

    public void clear(String obj) {

    }

    @Override
    public void validate_table_count(String obj, String text) {

    }

    @Override
    public void validate_table_column_value(String obj, String column, String value) {

    }

    @Override
    public void click_field_row_text(String obj, String column, String value) {

    }

    @Override
    public void click_options(String obj, String value) {

    }

    @Override
    public void gridLayout_actions(String action, String value) {

    }

    @Override
    public void validate_room_layout_property(String property, String text) throws IOException {

    }

    @Override
    public void validate_text_contains(String obj, String text) {

    }

    @Override
    public void validate_all_table_column_values(String obj, String column, String value) {

    }

    @Override
    public void click_results_table_value_by_key(String obj, String searchColumn, String clickColumn, String value) {

    }

    @Override  @Then("Secret - set-variable - key: {string} value: {string} timestamp: {string} random: {string}")
    public void set_variable(String key, String val, String timestamp, String random) {
        BasePage.set_variable(cucumber.pages.Secret.variableMap, key, val, timestamp, random);
    }

    public void validate_table_value(String obj, String column, String row, String value) {

    }

    @Override @Then("Secret - check-search-table-value - obj: {string} column: {string} row: {string} value: {string} variable: {string}")
    public void check_search_table_value(String obj, String column, String row, String value, String isVariable) {
        BasePage.check_search_table_value(Table.parse(cucumber.pages.Secret.getElement(obj)),column, row, value, isVariable,cucumber.pages.Secret.variableMap);
    }
}
