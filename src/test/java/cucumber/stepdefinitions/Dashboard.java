package cucumber.stepdefinitions;

import cucumber.BasePage;
import cucumber.BaseStep;
import cucumber.Config;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class Dashboard implements BaseStep {

    @Then("Dashboard - Rearrange widgets")
    public void rearrange_widgets() {

        String testMessage = "Last widget was moved to the first widget.";

        List<WebElement> childrenA = cucumber.pages.Dashboard.getElement("myDashboard").findElements(By.xpath("li"));
        String srcKey = childrenA.get(childrenA.size() - 1).getAttribute("id");
        String destKey = childrenA.get(0).getAttribute("id");

        HashMap<String, By> pageMap = cucumber.pages.Dashboard.getPageMap();

        cucumber.pages.Dashboard.dragAndDrop(pageMap.get(srcKey), pageMap.get(destKey));

        List<WebElement> childrenB = cucumber.pages.Dashboard.getElement("myDashboard").findElements(By.xpath("li"));

        Assert.assertEquals(srcKey, childrenB.get(0).getAttribute("id"), testMessage);

    }

    private String path = Config.get("dashboard_path");

    @Given("Dashboard - Navigate")
    public void navigate() {
        cucumber.pages.Dashboard.navigate(path);
    }

    //----------------------SHARED BEGIN

    @Override @Then("Dashboard - type - obj: {string} text: {string} isVariable: {string}")
    public void type(String text, String obj, String isVariable) {
        BasePage.type(cucumber.pages.Dashboard.getElement(obj), text, isVariable, cucumber.pages.Dashboard.variableMap);
    }

    @Override @Then("Dashboard - click - obj: {string}")
    public void click(String obj) {
        BasePage.click(cucumber.pages.Dashboard.getElement(obj));
    }

    @Override @Then("Dashboard - isDisplayed - obj: {string}")
    public void is_displayed(String obj) {
        BasePage.is_displayed(cucumber.pages.Dashboard.getElement(obj));
    }

    @Override
    public void select(String obj, String text) {

    }

    @Override
    public void check_attribute(String obj, String attribute, String value) {

    }

    @Override
    public void check_search_table_value(String obj, String column, String row, String value, String isVariable) {

    }

    @Override
    public void click_search_table_value(String obj, String column, String row) {

    }

    @Override
    public void validate_text(String obj, String value) {

    }

    @Override
    public void validate_table_column_header(String obj, String columnIndex, String value) {

    }

    @Override
    public void set_variable(String key, String val, String timestamp, String random) {

    }

    @Override
    public void hover(String obj) {

    }

    @Override
    public void validate_title(String text) {

    }

    @Override
    public void select_table_value(String obj, String column, String row, String value, String isVariable) {

    }

    @Override
    public void validate_table_value(String obj, String column, String row, String value) {

    }

    @Override
    public void clear(String obj) {

    }

    @Override
    public void validate_table_count(String obj, String text) {

    }

    @Override
    public void validate_table_column_value(String obj, String column, String value) {

    }

    @Override
    public void click_field_row_text(String obj, String column, String value) {

    }

    @Override
    public void click_options(String obj, String value) {

    }

    @Override
    public void gridLayout_actions(String action, String value) {

    }

    @Override
    public void validate_room_layout_property(String property, String text) throws IOException {

    }

    @Override
    public void validate_text_contains(String obj, String text) {

    }

    @Override
    public void validate_all_table_column_values(String obj, String column, String value) {

    }

    @Override
    public void click_results_table_value_by_key(String obj, String searchColumn, String clickColumn, String value) {

    }
}
