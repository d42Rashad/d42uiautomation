package cucumber.stepdefinitions;

import cucumber.*;
import io.cucumber.java.en.Then;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.io.IOException;
import java.sql.*;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class VServer implements BaseStep {

    @Override @Then("VServer - type - obj: {string} text: {string} isVariable: {string}")
    public void type(String obj, String text, String isVariable) {
//        BasePage.type(cucumber.pages.VServer.getElement(obj), text, isVariable, cucumber.pages.VServer.variableMap);
        BasePage.type(cucumber.pages.VServer.getElement(obj), text, isVariable, cucumber.pages.VServer.variableMap);
    }

    @Override @Then("VServer - click - obj: {string}")
    public void click(String obj) {
        BasePage.click(cucumber.pages.VServer.getElement(obj));
    }

    @Override @Then("VServer - isDisplayed - obj: {string}")
    public void is_displayed(String obj) {
        BasePage.is_displayed(cucumber.pages.VServer.getElement(obj));
    }

    @Override @Then("VServer - select - obj: {string} value: {string}")
    public void select(String obj, String text) {
        BasePage.select(cucumber.pages.VServer.getElement(obj), text);
    }

    @Override
    public void check_attribute(String obj, String attribute, String value) {

    }

    @Override @Then("VServer - check-search-table-value - obj: {string} column: {string} row: {string} value: {string} variable: {string}")
    public void check_search_table_value(String obj, String column, String row, String value, String isVariable) {
        BasePage.check_search_table_value(Table.parse(cucumber.pages.VServer.getElement(obj)),column, row, value, isVariable,cucumber.pages.VServer.variableMap);
    }

    @Override @Then("VServer - click-search-table-value - obj: {string} column: {string} row: {string}")
    public void click_search_table_value(String obj, String column, String row) {
        BasePage.click_search_table_value(Table.parse(cucumber.pages.VServer.getElement(obj)), row, column);
    }

    @Override @Then("VServer - validate-text - obj: {string} value: {string}")
    public void validate_text(String obj, String value) {
        BasePage.assertEquals(cucumber.pages.VServer.getElement(obj).getText(), value);
    }

    @Override
    public void validate_table_column_header(String obj, String columnIndex, String value) {

    }

    @Override  @Then("VServer - set-variable - key: {string} value: {string} timestamp: {string} random: {string}")
    public void set_variable(String key, String val, String timestamp, String random) {
        BasePage.set_variable(cucumber.pages.VServer.variableMap, key, val, timestamp, random);
    }

    @Override
    public void hover(String obj) {

    }

    @Override @Then("VServer - validate-title - text: {string}")
    public void validate_title(String text) {
        BasePage.assertEquals(cucumber.pages.VServer.driver().getTitle(), text);
    }

    @Override
    public void select_table_value(String obj, String column, String row, String value, String isVariable) {

    }

    @Override
    public void validate_table_value(String obj, String column, String row, String value) {

    }

    @Override
    public void clear(String obj) {

    }

    @Override
    public void validate_table_count(String obj, String text) {

    }

    @Override
    public void validate_table_column_value(String obj, String column, String value) {

    }

    @Override
    public void click_field_row_text(String obj, String column, String value) {

    }

    @Override
    public void click_options(String obj, String value) {

    }

    @Override
    public void gridLayout_actions(String action, String value) {

    }

    @Override
    public void validate_room_layout_property(String property, String text) throws IOException {

    }

    @Override
    public void validate_text_contains(String obj, String text) {

    }

    @Override
    public void validate_all_table_column_values(String obj, String column, String value) {

    }

    @Override
    public void click_results_table_value_by_key(String obj, String searchColumn, String clickColumn, String value) {

    }

    @Then("VServer - wait-for-search-table-value-not-visible - obj: {string} column: {string} row: {string} value: {string} timeout {string} retries: {string}")
    public void wait_for_search_table_value_not_visible(String obj, String column, String row, String value, String timeout, String retries) throws InterruptedException {
        ArrayList<HashMap<String, WebElement>> map = Table.parse(cucumber.pages.VServer.getElement(obj));
        Boolean found = BasePage.isJobRunning(map, column, row, value);
        if(Integer.parseInt(retries) > 0 && found) {
            BasePage.driver().navigate().refresh();
            BasePage.click(cucumber.pages.VServer.getElement("searchButton"));
            Thread.sleep(Integer.parseInt(timeout) * 1000);
            wait_for_search_table_value_not_visible(obj, column, row, value, timeout, String.valueOf(Integer.parseInt(retries)-1));
        }
        String expected = value;
        String actual = map.get(Integer.parseInt(row)-1).get(column).getText();
        Assert.assertFalse(actual.trim().equals(expected), "Validating that the job has finished.");
    }

    @Then("VServer - persist-doql - view: {string} key: {string} jobName: {string} rc_name: {string} ip_address: {string}")
    public void persist_doql(String view, String key, String jobName, String rc_name, String ip_address) throws IOException, SQLException, InterruptedException {
        String data = Web.getViewJobScoreV1Performance(view, key);
        Database d = new Database();
        String[] args = data.split(",");
        d.view.task_type = args[0].trim();
        d.view.basic_sendrc = Timestamp.from(d.getInstant(args[1].trim()));
        d.view.basic_runstart = Timestamp.from(d.getInstant(args[2].trim()));
        d.view.basic_runend = Timestamp.from(d.getInstant(args[3].trim()));
        d.view.basic_queueend = Timestamp.from(d.getInstant(args[4].trim()));
        d.view.detailed_sendrc = Timestamp.from(d.getInstant(args[5].trim()));
        d.view.detailed_runstart = Timestamp.from(d.getInstant(args[6].trim()));
        d.view.detailed_runend = Timestamp.from(d.getInstant(args[7].trim()));
        d.view.detailed_queueend = Timestamp.from(d.getInstant(args[8].trim()));
        d.view.job_name = jobName;
        d.view.d42_build_no = Config.get("version");
        d.view.port_check_failed = Integer.parseInt(cucumber.pages.VServer.variableMap.get("Port check failed"));
        d.view.auth_failed = Integer.parseInt(cucumber.pages.VServer.variableMap.get("Auth failed"));
        d.view.discovery_exception = Integer.parseInt(cucumber.pages.VServer.variableMap.get("Discovery Exception"));
        d.view.success = Integer.parseInt(cucumber.pages.VServer.variableMap.get("Success"));
        d.view.total_attempted = Integer.parseInt(cucumber.pages.VServer.variableMap.get("Total attempted"));

        d.view.rc_name = rc_name;
        d.view.ip_address = ip_address;

        d.insertRow(d.view);
    }

    @Then("VServer - wait-for-job-complete - view: {string} key: {string} jobName: {string} timeout {string} retries: {string}")
    public void wait_for_job_complete(String view, String key, String jobName, String timeout, String retries) throws IOException, SQLException, InterruptedException {
        String data = Web.getViewJobScoreV1Performance(view, key);
        String[] args = data.split(",");
        if(Integer.parseInt(retries) > 0 && stringArrayContainsEmptyValue(args)) {
            BasePage.driver().navigate().refresh();
            Thread.sleep(Integer.parseInt(timeout) * 1000);
            wait_for_job_complete(view, key, jobName, timeout, String.valueOf(Integer.parseInt(retries)-1));
        }
    }

    public Boolean stringArrayContainsEmptyValue(String[] args){
//        System.out.println("---stringArrayContainsEmptyValue---");
        for(String x : args) {
//            System.out.println("x--->>>>>" + x.trim());
            if(x.trim().isEmpty()){
                return true;
            }
        }
        return false;
    }

    //TODO: move to BasePage
    @Then("VServer - toggle - obj: {string} value: {string}")
    public void toggle(String obj, String value) {
        if(cucumber.pages.VServer.getElement(obj).getText().trim().equals("Job Status (" + value + ")")) {
            cucumber.pages.VServer.getElement(obj).findElement(By.xpath("a")).click();
            BasePage.waitForVisible(cucumber.pages.VServer.getElement(obj).findElements(By.xpath("*")));
        }
    }

    //TODO: move to BasePage
    @Then("VServer - set-variable-from-object-text - obj: {string} value: {string} index: {string}")
    public void set_variable_from_object_text(String obj, String value, String index) {
        int idx = Integer.parseInt(index) - 1;
        WebElement table = cucumber.pages.VServer.getElement(obj).findElements(By.xpath("table")).get(idx);
        WebElement row = table.findElement(By.xpath("tbody/tr/td[text()='" + value + "']"));
        WebElement val = row.findElement(By.xpath("following-sibling::td"));
        cucumber.pages.VServer.variableMap.put(value,val.getText());
    }
}












































