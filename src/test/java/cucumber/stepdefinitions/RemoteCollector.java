package cucumber.stepdefinitions;

import cucumber.BasePage;
import cucumber.BaseStep;
import cucumber.Table;
import cucumber.Wait;
import io.cucumber.java.en.Then;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RemoteCollector implements BaseStep {

    @Then("RemoteCollector - check-search-table-cell-attribute-by-column - column: {string} value: {string} count: {string} timeout: {string} retries: {string}")
    public static void check_search_table_cell_attribute_by_column(String column, String value, String count, String timeout, String retries) throws InterruptedException {
        BasePage.check_search_table_cell_attribute_by_column(Table.parse(cucumber.pages.RemoteCollector.getElement("result_listTable")), column, value, count, timeout, retries, "span", "style");
    }

    @Override @Then("RemoteCollector - type - obj: {string} text: {string} isVariable: {string}")
    public void type(String obj, String text, String isVariable) {
        BasePage.type(cucumber.pages.RemoteCollector.getElement(obj), text, isVariable, cucumber.pages.RemoteCollector.variableMap);
    }

    @Override @Then("RemoteCollector - click - obj: {string}")
    public void click(String obj) {
        BasePage.click(cucumber.pages.RemoteCollector.getElement(obj));
    }

    @Override @Then("RemoteCollector - isDisplayed - obj: {string}")
    public void is_displayed(String obj) {
        BasePage.is_displayed(cucumber.pages.RemoteCollector.getElement(obj));
    }

    @Override @Then("RemoteCollector - select - obj: {string} value: {string}")
    public void select(String obj, String text) {
        BasePage.select(cucumber.pages.RemoteCollector.getElement(obj), text);
    }

    @Override
    public void check_attribute(String obj, String attribute, String value) {

    }

    @Override @Then("RemoteCollector - check-search-table-value - obj: {string} column: {string} row: {string} value: {string} variable: {string}")
    public void check_search_table_value(String obj, String column, String row, String value, String isVariable) {
        BasePage.check_search_table_value(Table.parse(cucumber.pages.RemoteCollector.getElement(obj)),column, row, value, isVariable,cucumber.pages.RemoteCollector.variableMap);
    }

    @Override
    public void click_search_table_value(String obj, String column, String row) {

    }

    @Override @Then("RemoteCollector - validate-text - obj: {string} value: {string}")
    public void validate_text(String obj, String value) {
        BasePage.assertEquals(cucumber.pages.RemoteCollector.getElement(obj).getText(), value);
    }

    @Override
    public void validate_table_column_header(String obj, String columnIndex, String value) {

    }

    @Override  @Then("RemoteCollector - set-variable - key: {string} value: {string} timestamp: {string} random: {string}")
    public void set_variable(String key, String val, String timestamp, String random) {
        BasePage.set_variable(cucumber.pages.RemoteCollector.variableMap, key, val, timestamp, random);
    }

    @Override
    public void hover(String obj) {

    }

    @Override @Then("RemoteCollector - validate-title - text: {string}")
    public void validate_title(String text) {
        BasePage.assertEquals(cucumber.pages.RemoteCollector.driver().getTitle(), text);
    }

    @Override
    public void select_table_value(String obj, String column, String row, String value, String isVariable) {

    }

    @Override
    public void validate_table_value(String obj, String column, String row, String value) {

    }

    @Override
    public void clear(String obj) {

    }

    @Override
    public void validate_table_count(String obj, String text) {

    }

    @Override
    public void validate_table_column_value(String obj, String column, String value) {

    }

    @Override
    public void click_field_row_text(String obj, String column, String value) {

    }

    @Override
    public void click_options(String obj, String value) {

    }

    @Override
    public void gridLayout_actions(String action, String value) {

    }

    @Override
    public void validate_room_layout_property(String property, String text) throws IOException {

    }

    @Override
    public void validate_text_contains(String obj, String text) {

    }

    @Override
    public void validate_all_table_column_values(String obj, String column, String value) {

    }

    @Override
    public void click_results_table_value_by_key(String obj, String searchColumn, String clickColumn, String value) {

    }

    @Then("RemoteCollector - validate-wds-status - rc: {string} timeout: {string} retries: {string}")
    public void validate_wds_status(String rc_name, String timeout, String retries) throws InterruptedException {
        ArrayList<Map<String, WebElement>> tableMap =  Table.parseTable(cucumber.pages.RemoteCollector.getElement("resultList"));
        Boolean found = false;
        String attributeValue = "color: green;";
        for(Map<String, WebElement> row : tableMap) {
            if(row.get("Name").getText().equals(rc_name)) {
                for(WebElement element : row.get("WDS").findElements(By.xpath("span"))) {
                    if(element.getAttribute("style").equals(attributeValue)) {
                        found = true;
                    }
                }
            }
        }

        if(Integer.parseInt(retries) > 0 && !found) {
            Thread.sleep(Integer.parseInt(timeout) * 1000);
            BasePage.driver().navigate().refresh();
            validate_wds_status(rc_name, timeout, String.valueOf(Integer.parseInt(retries) - 1));
        }
        Assert.assertTrue(found, "Validating that there is a connected WDS to a given RC.");
    }
}
