package cucumber.stepdefinitions;

import cucumber.BasePage;
import cucumber.BaseStep;
import cucumber.Table;
import io.cucumber.java.en.Then;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Rack implements BaseStep {

    @Override @Then("Rack - type - obj: {string} text: {string} isVariable: {string}")
    public void type(String obj, String text, String isVariable) {
        BasePage.type(cucumber.pages.Rack.getElement(obj), text, isVariable, cucumber.pages.Rack.variableMap);
    }

    @Override @Then("Rack - click - obj: {string}")
    public void click(String obj) {
        BasePage.click(cucumber.pages.Rack.getElement(obj));
    }

    @Override @Then("Rack - isDisplayed - obj: {string}")
    public void is_displayed(String obj) {
        BasePage.is_displayed(cucumber.pages.Rack.getElement(obj));
    }

    @Override @Then("Rack - select - obj: {string} value: {string}")
    public void select(String obj, String text) {
        BasePage.select(cucumber.pages.Rack.getElement(obj), text);
    }

    @Override
    public void check_attribute(String obj, String attribute, String value) {

    }

    @Override @Then("Rack - check-search-table-value - obj: {string} column: {string} row: {string} value: {string} variable: {string}")
    public void check_search_table_value(String obj, String column, String row, String value, String isVariable) {
        BasePage.check_search_table_value(Table.parse(cucumber.pages.Rack.getElement(obj)),column, row, value, isVariable,cucumber.pages.Rack.variableMap);
    }

    @Override @Then("Rack - click-search-table-value - obj: {string} column: {string} row: {string}")
    public void click_search_table_value(String obj, String column, String row) {
        BasePage.click_search_table_value(Table.parse(cucumber.pages.Rack.getElement(obj)), row, column);
    }

    @Override @Then("Rack - validate-text - obj: {string} value: {string}")
    public void validate_text(String obj, String value) {
        BasePage.assertEquals(cucumber.pages.Rack.getElement(obj).getText(), value);
    }

    @Override
    public void validate_table_column_header(String obj, String columnIndex, String value) {

    }

    @Override  @Then("Rack - set-variable - key: {string} value: {string} timestamp: {string} random: {string}")
    public void set_variable(String key, String val, String timestamp, String random) {
        BasePage.set_variable(cucumber.pages.Rack.variableMap, key, val, timestamp, random);
    }

    @Override
    public void hover(String obj) {

    }

    @Override @Then("Rack - validate-title - text: {string}")
    public void validate_title(String text) {
        BasePage.assertEquals(cucumber.pages.Rack.driver().getTitle(), text);
    }

    @Override
    public void select_table_value(String obj, String column, String row, String value, String isVariable) {

    }

    @Override
    public void validate_table_value(String obj, String column, String row, String value) {

    }

    @Override @Then("Rack - clear - obj: {string}")
    public void clear(String obj) {
        BasePage.clear(cucumber.pages.Rack.getElement(obj));
    }

    @Override @Then("Rack - validate-table-count - obj: {string} value: {string}")
    public void validate_table_count(String obj, String text) {
        BasePage.assertEquals(cucumber.pages.Rack.getElement(obj).findElements(By.xpath("tbody/tr")).size(), Integer.parseInt(text));
    }

    @Override @Then("Rack - validate-text-contains - obj: {string} value: {string}")
    public void validate_text_contains(String obj, String text) {
        BasePage.contains(cucumber.pages.Rack.getElement(obj).getText(), text);
    }

    @Override @Then("Rack - click-field-row-text - obj: {string} column: {string} value: {string}")
    public void click_field_row_text(String obj, String column, String value) {
        BasePage.click_field_row_text(cucumber.pages.Rack.getElement(obj), column, value);
    }

    @Override
    public void click_options(String obj, String value) {

    }

    @Override
    public void gridLayout_actions(String action, String value) {

    }

    @Override
    public void validate_room_layout_property(String property, String text) throws IOException {

    }

    @Override  @Then("Rack - validate-table-column-value - obj: {string} column: {string} value: {string}")
    public void validate_table_column_value(String obj, String column, String value) {
        BasePage.validate_table_column_value(cucumber.pages.Rack.getElement(obj).findElements(By.xpath("tbody/tr/td[@class='" + column + "']/p/a")), value);
    }

    @Override @Then("Rack - validate-all-table-column-values - obj: {string} column: {string} value: {string}")
    public void validate_all_table_column_values(String obj, String column, String value) {
        BasePage.validate_all_table_column_valuesx(Table.parse(cucumber.pages.Rack.getElement(obj)), column, value);
    }

    @Override @Then("Rack - click-results-table-value-by-key - obj: {string} searchColumn: {string} clickColumn: {string} value: {string}")
    public void click_results_table_value_by_key(String obj, String searchColumn, String clickColumn, String value) {
        BasePage.click_results_table_value_by_key(Table.parse(cucumber.pages.Rack.getElement(obj)),searchColumn, clickColumn, value);
    }

    @Then("Rack - drag-rack-selection - from: {string} to: {string} index: {string}")
    public void rackDragFromTo(String from, String to, String index) {
        WebElement w = cucumber.pages.Rack.getElement("drawingRack").findElement(By.xpath("//*[local-name()='svg']/*[local-name()='text']/*[text()='" + from + "']"));
        WebElement s = cucumber.pages.Rack.getElement("drawingRack").findElements(By.xpath("//*[local-name()='svg']/*[local-name()='text'][@text-anchor='middle']/*[text()='" + to + "']")).get(Integer.parseInt(index)-1);
        BasePage.dragAndDrop(w,s);
    }

    @Then("Rack - click-rack-selection - obj: {string}")
    public void click_rack_selection(String text) {
        cucumber.pages.Rack.rackSelection(text).click();
    }

    @Then("Rack - hover-rack-device - obj: {string}")
    public void hover_rack_device(String text) {
        BasePage.hover(cucumber.pages.Rack.rackSelection(text));
    }

    @Then("Rack - check-property-rack-popup-field - obj: {string} value: {string}")
    public void check_property_rack_popup_field(String text, String value) {
        BasePage.assertEquals(cucumber.pages.Rack.rackDevicePopupField(text).getText().split(":")[1].trim(), value.trim());
    }
}












