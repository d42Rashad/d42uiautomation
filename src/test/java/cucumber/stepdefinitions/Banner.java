package cucumber.stepdefinitions;

import cucumber.BasePage;
import cucumber.BaseStep;
import io.cucumber.java.en.Then;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.IOException;

public class Banner implements BaseStep {

    @Then("Banner - navigation-left-menu - path: {string}")
    public void navigation_left_menu(String path) {
        String[] pathways = path.split(">");
        WebElement menu = cucumber.pages.Banner.getElement("navigationMenu");
        WebElement leftMenu = menu.findElement(By.xpath("//div[@class='navigation-leftmenu']"));

        WebElement menuItem = null;
        WebElement subElement = null;

        WebElement selectedMenu = leftMenu.findElement(By.xpath("li/a/span[text()='" + pathways[0].trim() + "']//parent::a"));
        BasePage.hover(selectedMenu);

        if (pathways.length > 1) {
            menuItem = selectedMenu.findElement(menuItemBy(pathways[1].trim()));
            BasePage.hover(menuItem);
        }

        if (pathways.length > 2) {
            subElement = menuItem.findElement(menuItemBy(pathways[2].trim()));
            BasePage.hover(menuItem);
        }

        switch(pathways.length) {
            case 1:
                BasePage.click(selectedMenu);
                break;
            case 2:
                BasePage.click(menuItem);
                break;
            case 3:
                BasePage.click(subElement);
                break;
        }
    }

    private By menuItemBy(String text) {
        return By.xpath("following-sibling::ul/li/a/span[text()='" + text + "']//parent::a");
    }


    @Override @Then("Banner - type - obj: {string} text: {string} isVariable: {string}")
    public void type(String obj, String text, String isVariable) {
        BasePage.type(cucumber.pages.Banner.getElement(obj), text, isVariable, cucumber.pages.Banner.variableMap);
    }

    @Override @Then("Banner - click - obj: {string}")
    public void click(String obj) {
        BasePage.click(cucumber.pages.Banner.getElement(obj));
    }

    @Override @Then("Banner - isDisplayed - obj: {string}")
    public void is_displayed(String obj) {
        BasePage.is_displayed(cucumber.pages.Banner.getElement(obj));
    }

    @Override @Then("Banner - select - obj: {string} value: {string}")
    public void select(String obj, String text) {
        BasePage.select(cucumber.pages.Banner.getElement(obj), text);
    }

    @Override
    public void check_attribute(String obj, String attribute, String value) {

    }

    @Override
    public void check_search_table_value(String obj, String column, String row, String value, String isVariable) {

    }

    @Override
    public void click_search_table_value(String obj, String column, String row) {

    }

    @Override
    public void validate_text(String obj, String value) {

    }

    @Override
    public void validate_table_column_header(String obj, String columnIndex, String value) {

    }

    @Override
    public void set_variable(String key, String val, String timestamp, String random) {

    }

    @Override @Then("Banner - hover - obj: {string}")
    public void hover(String obj) {
        BasePage.hover(cucumber.pages.Banner.getElement(obj));
    }

    @Override @Then("Banner - validate-title - text: {string}")
    public void validate_title(String text) {
        BasePage.validate_title(text);
    }

    @Override
    public void select_table_value(String obj, String column, String row, String value, String isVariable) {

    }

    @Override
    public void validate_table_value(String obj, String column, String row, String value) {

    }

    @Override
    public void clear(String obj) {

    }

    @Override
    public void validate_table_count(String obj, String text) {

    }

    @Override
    public void validate_table_column_value(String obj, String column, String value) {

    }

    @Override
    public void click_field_row_text(String obj, String column, String value) {

    }

    @Override
    public void click_options(String obj, String value) {

    }

    @Override
    public void gridLayout_actions(String action, String value) {

    }

    @Override
    public void validate_room_layout_property(String property, String text) throws IOException {

    }

    @Override
    public void validate_text_contains(String obj, String text) {

    }

    @Override
    public void validate_all_table_column_values(String obj, String column, String value) {

    }

    @Override
    public void click_results_table_value_by_key(String obj, String searchColumn, String clickColumn, String value) {

    }
}
