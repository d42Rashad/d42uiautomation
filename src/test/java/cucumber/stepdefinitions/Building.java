package cucumber.stepdefinitions;

import cucumber.BasePage;
import cucumber.BaseStep;
import cucumber.Table;
import io.cucumber.java.en.Then;
import org.openqa.selenium.By;

import java.io.IOException;

public class Building implements BaseStep {

    @Override @Then("Building - type - obj: {string} text: {string} isVariable: {string}")
    public void type(String obj, String text, String isVariable) {
        BasePage.type(cucumber.pages.Building.getElement(obj), text, isVariable, cucumber.pages.Building.variableMap);
    }

    @Override @Then("Building - click - obj: {string}")
    public void click(String obj) {
        BasePage.click(cucumber.pages.Building.getElement(obj));
    }

    @Override @Then("Building - isDisplayed - obj: {string}")
    public void is_displayed(String obj) {
        BasePage.is_displayed(cucumber.pages.Building.getElement(obj));
    }

    @Override @Then("Building - select - obj: {string} value: {string}")
    public void select(String obj, String text) {
        BasePage.select(cucumber.pages.Building.getElement(obj), text);
    }

    @Override @Then("Building - check-attribute - obj: {string} attribute: {string} value: {string}")
    public void check_attribute(String obj, String attribute, String value) {
        BasePage.check_attribute(cucumber.pages.Building.getElement(obj),attribute,value);
    }

    @Override @Then("Building - click-search-table-value - obj: {string} column: {string} row: {string}")
    public void click_search_table_value(String obj, String column, String row) {
        BasePage.click_search_table_value(Table.parse(cucumber.pages.Building.getElement(obj)), row, column);
    }

    @Override @Then("Building - validate-text - obj: {string} value: {string}")
    public void validate_text(String obj, String value) {
        BasePage.assertEquals(cucumber.pages.Building.getElement(obj).getText(), value);
    }

    @Override @Then("Building - validate-table-column-header - obj: {string} index: {string} value: {string}")
    public void validate_table_column_header(String obj, String columnIndex, String value) {
        BasePage.validate_column_header(cucumber.pages.Building.getElement(obj),columnIndex,value);
    }

    @Override @Then("Building - hover - obj: {string}")
    public void hover(String obj) {
        BasePage.hover(cucumber.pages.Building.getElement(obj));
    }

    @Override @Then("Building - validate-title - text: {string}")
    public void validate_title(String text) {
        BasePage.validate_title(text);
    }

    @Override @Then("Building - select-table-value - obj: {string} column: {string} row: {string} value: {string} variable: {string}")
    public void select_table_value(String obj, String column, String row, String value, String isVariable) {
        BasePage.select_table_value(cucumber.pages.Building.getElement(obj),cucumber.pages.Building.variableMap,column,row,value,isVariable);
    }

    @Override  @Then("Building - clear - obj: {string}")
    public void clear(String obj) {
        BasePage.clear(cucumber.pages.Building.getElement(obj));
    }

    @Override
    public void validate_table_count(String obj, String text) {

    }

    @Override  @Then("Building - set-variable - key: {string} value: {string} timestamp: {string} random: {string}")
    public void set_variable(String key, String val, String timestamp, String random) {
        BasePage.set_variable(cucumber.pages.Building.variableMap, key, val, timestamp, random);
    }

    @Override  @Then("Building - validate-table-value - obj: {string} column: {string} row: {string} value: {string}")
    public void validate_table_value(String obj, String column, String row, String value) {
        BasePage.validate_table_value(Table.parseDynamicTable(cucumber.pages.Building.getElement(obj), Integer.parseInt(row)-1 , "p", column), value);
    }

    @Override @Then("Building - check-search-table-value - obj: {string} column: {string} row: {string} value: {string} variable: {string}")
    public void check_search_table_value(String obj, String column, String row, String value, String isVariable) {
        BasePage.check_search_table_value(Table.parse(cucumber.pages.Building.getElement(obj)),column, row, value, isVariable,cucumber.pages.Building.variableMap);
    }

    @Override  @Then("Building - validate-table-column-value - obj: {string} column: {string} value: {string}")
    public void validate_table_column_value(String obj, String column, String value) {
        BasePage.validate_table_column_value(cucumber.pages.Building.getElement(obj).findElements(By.xpath("tbody/tr/td[@class='" + column + "']/p/a")), value);
    }

    @Override
    public void click_field_row_text(String obj, String column, String value) {

    }

    @Override
    public void click_options(String obj, String value) {

    }

    @Override
    public void gridLayout_actions(String action, String value) {

    }

    @Override
    public void validate_room_layout_property(String property, String text) throws IOException {

    }

    @Override
    public void validate_text_contains(String obj, String text) {

    }

    @Override
    public void validate_all_table_column_values(String obj, String column, String value) {

    }

    @Override
    public void click_results_table_value_by_key(String obj, String searchColumn, String clickColumn, String value) {

    }

}

























