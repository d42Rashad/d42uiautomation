package cucumber;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

public class BaseTest {

    private WebDriver driver;

    @Before
    public void setup() {
        driver = DriverFactory.getInstance().getDriver();
    }

    @After
    public void breakdown(Scenario scenario) throws IOException {
        String testCaseKey = parseTestCaseKey(scenario.getName().split("\\\\s+")[0]);
        String status = "Pass";

        if(scenario.isFailed()) {
            byte[] screenShot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenShot, "image/png", scenario.getName());
            status = "Fail";
        }

        postRestResult(testCaseKey, status);

        DriverFactory.getInstance().removeDriver();
    }

    private String parseTestCaseKey(String testCaseKey) {
        if(Boolean.parseBoolean(Config.get("postToAdaptavist"))) {
            testCaseKey = testCaseKey.substring(testCaseKey.indexOf("[") + 1);
            testCaseKey = testCaseKey.substring(0, testCaseKey.indexOf("]"));
        }
        return testCaseKey;
    }

    private void postRestResult(String testCaseKey, String status) throws IOException {
        Web.postAdaptavistResults(new Adaptavist(testCaseKey, status).apl);
    }

}
