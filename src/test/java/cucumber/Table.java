package cucumber;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Table {

    public static String getColumnHeader(WebElement tableElement, int index) {
        // create empty table object and iterate through all rows of the found table element
        ArrayList<HashMap<String, WebElement>> userTable = new ArrayList<HashMap<String, WebElement>>();
        List<WebElement> rowElements = tableElement.findElements(By.xpath(".//tr"));
        // get column names of table from table headers
        ArrayList<String> columnNames = new ArrayList<String>();
        List<WebElement> headerElements = rowElements.get(0).findElements(By.xpath(".//th"));
        for (WebElement headerElement : headerElements) {
            if(!headerElement.getText().isEmpty()) {
                columnNames.add(headerElement.getText());
            }
        }
        return columnNames.get(index);
    }

    public static ArrayList<HashMap<String, WebElement>> parse(WebElement element) {
        ArrayList<String> columnList = new ArrayList<>();
        for (WebElement e : element.findElements(By.xpath("thead/tr")).get(0).findElements(By.xpath("th/div[@class='text']"))){
//            if(e.getText().isEmpty()) {
//                columnList.add("checkbox");
//            } else {
//                columnList.add(e.getText());
//            }
            if(e.getText().isEmpty()) {
                if (e.findElements(By.xpath("span/*")).size() > 0){
                    columnList.add("checkbox");
                } else {
                    columnList.add("commands");
                }
            } else {
                columnList.add(e.getText());
            }

        }
        ArrayList<HashMap<String, WebElement>> returnValue = new ArrayList<HashMap<String, WebElement>>();
        for (WebElement row : element.findElements(By.xpath("tbody/tr"))) {
            HashMap<String, WebElement> rowTable = new HashMap<String, WebElement>();
            int counter = 0;
            for (WebElement col : row.findElements(By.xpath("*"))) {
                if (col.findElements(By.xpath("*")).size() > 0) {
                    if (col.getAttribute("class").equals("action-checkbox")) {
                        rowTable.put("Checkbox", col.findElement(By.xpath("input[@name='_selected_action']")));
                    } else if(col.getTagName().equals("th") || col.getTagName().equals("td")) {
                        rowTable.put(columnList.get(counter), col.findElement(By.xpath("*")));
                    }
                } else {
                    rowTable.put(columnList.get(counter),col);
                }
                counter++;
            }
            returnValue.add(rowTable);
        }
        return returnValue;
    }

    public static List<WebElement> getTableColumns(WebElement table) {
        WebElement columnRow = table.findElement(By.xpath("thead/tr[@class='sticky fixed-header']"));
        return columnRow.findElements(By.xpath("th"));
    }

    public static List<WebElement> getTableRows(WebElement table) {
        return table.findElements(By.xpath("tbody/tr"));
    }

    public static WebElement parseColumn(WebElement element) {
        switch(element.getTagName()){
            case "span":
                return parseColumnSpan(element);
            case "a":
                return parseColumnAnchor(element);
        }
        return null;
    }

    public static WebElement parseColumnSpan(WebElement element) {
        List<WebElement> childrenElements = element.findElements(By.xpath("*"));
        if(childrenElements.size() == 0) {
            return element;
        } else {
            return element.findElement(By.xpath("input"));
        }
    }

    public static WebElement parseColumnAnchor(WebElement element) {
        return element;
    }

    public static ArrayList<Map<String, WebElement>> parseTable(WebElement table) {
        List<WebElement> columnContainers = getTableColumns(table);
        List<String> columnHeaders = new ArrayList<>();
        List<WebElement> rowContainers = getTableRows(table);
        ArrayList<Map<String, WebElement>> returnValue = new ArrayList<Map<String, WebElement>>();

        for(WebElement ele : columnContainers) {
            for(WebElement e : ele.findElements(By.xpath("div[@class='text']/*"))) {
                if(parseColumn(e).getText().trim().isEmpty()) {
                    columnHeaders.add(parseColumn(e).getTagName().trim());
                } else {
                    columnHeaders.add(parseColumn(e).getText().trim());
                }
            }
        }

        for(WebElement e : rowContainers) {
            List<WebElement> rowData = e.findElements(By.xpath("*"));
            returnValue.add(zipToMap(columnHeaders,rowData));
        }

        return returnValue;
    }

    public static <K, V> Map<K, V> zipToMap(List<K> keys, List<V> values) {
        return IntStream.range(0, keys.size()).boxed()
                .collect(Collectors.toMap(keys::get, values::get));
    }

    public static WebElement parseDynamicTable(WebElement element, int index, String obj, String columnClass) {
        return element.findElements(By.xpath("tbody/*")).get(index).findElement(By.xpath("td[@class='" + columnClass + "']/" + obj));
    }

}


























