package cucumber;

import cucumber.stepdefinitions.HealthStats;
import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class BasePage {

    public static HashMap<String, String> variableMap = new HashMap<String, String>();

    public static WebDriver driver() {
        return DriverFactory.getInstance().getDriver();
    }

    public static void navigate(String path) {
        driver().manage().window().maximize();
        driver().get(Config.get("host") + path);
    }

    public static WebElement findWebElement(By by) {
        waitForVisible(by);
        return driver().findElement(by);
    }

    public static void waitForVisible(By by) {
        WebDriverWait wait = new WebDriverWait(driver(), Long.parseLong(Config.get("implicit_wait")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public static void waitForVisible(List<WebElement> elements) {
        WebDriverWait wait = new WebDriverWait(driver(), Long.parseLong(Config.get("implicit_wait")));

        wait.until(ExpectedConditions.visibilityOfAllElements(elements));
    }

    public static void dragAndDrop(By source, By dest) {
        Actions actions = new Actions(driver());
        actions.dragAndDrop(driver().findElement(source), driver().findElement(dest)).perform();
    }

    public static void dragAndDrop(WebElement source, WebElement dest) {
        Actions actions = new Actions(driver());
        actions.dragAndDrop(source, dest).perform();
    }

    public static void select(WebElement source, String text) {
        Select s = new Select(source);
        s.selectByVisibleText(text);
    }

    public static void hover(WebElement source) {
        Actions  s = new Actions (driver());
        s.moveToElement(source).perform();
    }

    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public static By result_listTable() {
        return By.xpath("//table[@id='result_list']");
    }

    public static By pageHeader() {
        return By.xpath("//h1");
    }

    public static By saveButton() {
        return By.xpath("//input[@name='_save']");
    }

    public static By searchTextbox() {
        return By.xpath("//input[@id='searchbar']");
    }

    public static By continueButton() {
        return By.xpath("//input[@name='_continue']");
    }

    public static By searchButton() {
        return By.xpath("//input[@id='searchbar']/following-sibling::button");
    }

    public static By dropDownToolsList() {
        return By.xpath("//li[@id='dropdown-tools']");
    }

    public static By optionsButton() {
        return By.xpath("//li[@id='dropdown-tools']/a/i");
    }

    public static By editButton() {
        return By.xpath("//a[text()='Edit']");
    }

    public static By actionDropdown() {
        return By.xpath("//select[@name='action']");
    }

    public static By runSelectedActionButton() {
        return By.xpath("//button[@title='Run the selected action']");
    }

    public static void scrollElementIntoMiddle(WebElement element) {
        String scrollElementIntoMiddle = "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);"
                + "var elementTop = arguments[0].getBoundingClientRect().top;"
                + "window.scrollBy(0, elementTop-(viewPortHeight/2));";
        ((JavascriptExecutor) driver()).executeScript(scrollElementIntoMiddle, element);
    }

    public static HashMap<String, By> getBaseMap() {
        HashMap<String, By> pageMap = new HashMap<String, By>();
        pageMap.put("result_listTable", result_listTable());
        pageMap.put("pageHeader", pageHeader());
        pageMap.put("saveButton", saveButton());
        pageMap.put("searchTextbox", searchTextbox());
        pageMap.put("searchButton", searchButton());
        pageMap.put("continueButton", continueButton());
        pageMap.put("optionsButton", optionsButton());
        pageMap.put("dropDownToolsList", dropDownToolsList());
        pageMap.put("actionDropdown", actionDropdown());
        pageMap.put("runSelectedActionButton", runSelectedActionButton());
        pageMap.put("editButton", editButton());
        return pageMap;
    }

    public static boolean isClickable(WebElement element) {
        try
        {
            WebDriverWait wait = new WebDriverWait(driver(), 2);
            wait.until(ExpectedConditions.elementToBeClickable(element));
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    public static void setValue (WebElement element, String value) {
        //WebElement element = driver().findElement(By.xpath("enter the xpath here")); // you can use any locator
        JavascriptExecutor jse = (JavascriptExecutor)driver();
        jse.executeScript("arguments[0].value='" + value + "';", element);
    }

    public static String random () {
        return RandomStringUtils.randomAlphabetic(5).toString();
    }

    public static WebElement getElement(By by) {
        scrollElementIntoMiddle(findWebElement(by));
        return findWebElement(by);
    }

    public static void validate_table_column_value(List<WebElement> webElements, String value) {
        Boolean matchFound = false;
        for(WebElement element : webElements) {
            if (element.getText().equals(value)) {
                matchFound = true;
            }
        }
        Assert.assertTrue(matchFound, "checking if text exists in column.");
    }

    public static void validate_all_table_column_valuesx(ArrayList<HashMap<String, WebElement>> map, String column, String value) {
        Boolean diffFound = false;
        for(HashMap<String, WebElement> element : map) {
            if (!element.get(column).getText().equals(value)) {
                diffFound = true;
                break;
            }
        }
        Assert.assertFalse(diffFound, "checking if the text is equal to all values in column.");
    }

    public static HashMap<String, String> set_variable(HashMap<String, String> map, String key, String val, String timestamp, String random) {
        if(Boolean.parseBoolean(random)) {
            val = random();
        }
        if (Boolean.parseBoolean(timestamp)) {
            map.put(key, val + Instant.now().getEpochSecond());
        } else {
            map.put(key, val);
        }
        return map;
    }

    public static void assertEquals(String actual, String expected) {
        String testMessage = "Asserting: " + actual + " = " + expected;
        Assert.assertEquals(actual, expected, testMessage);
    }

    public static void assertEquals(int actual, int expected) {
        String testMessage = "Asserting: " + actual + " = " + expected;
        Assert.assertEquals(actual, expected, testMessage);
    }

    public static void contains(String actual, String expected) {
        String testMessage = "Asserting if : " + actual + " contains " + expected;
        Assert.assertEquals(actual.contains(expected), true, testMessage);
    }

    public static void click_search_table_value(ArrayList<HashMap<String, WebElement>> map, String row, String column) {
        map.get(Integer.parseInt(row)-1).get(column).click();
    }

    public static void wait_for_search_table_value_not_visible(ArrayList<HashMap<String, WebElement>> map, String column, String row, String value, String timeout, String retries, WebElement searchButton) throws InterruptedException {
        String expected = value;
        String actual = map.get(Integer.parseInt(row)-1).get(column).getText();
        if(Integer.parseInt(retries) > 0 && !actual.trim().equals(expected.trim())) {
            BasePage.click(searchButton);
            Thread.sleep(Integer.parseInt(timeout) * 1000);
            System.out.println("retrying");
            wait_for_search_table_value_not_visible(map, column, row, value, timeout, String.valueOf(Integer.parseInt(retries)-1), searchButton);
        }
        Assert.assertFalse(actual.trim().equals(expected), "Validating that the job has finished.");
    }

    public static Boolean isJobRunning(ArrayList<HashMap<String, WebElement>> map, String column, String row, String value) throws InterruptedException {
        String expected = value;
        String actual = map.get(Integer.parseInt(row)-1).get(column).getText();
        return actual.trim().equals(expected.trim());
    }

    public static void click_results_table_value_by_key(ArrayList<HashMap<String, WebElement>> map, String searchColumn, String clickColumn, String value) {
        for(HashMap<String, WebElement> m : map) {
            if(m.get(searchColumn).getText().equals(value)) {
                m.get(clickColumn).click();
            }
        }
    }

    public static void clear(WebElement element) {
        element.clear();
    }

    public static void type(WebElement element, String text, String isVariable, HashMap<String, String> variableMap) {
        if (Boolean.parseBoolean(isVariable)) {
            element.sendKeys(variableMap.get(text));
        } else {
            element.sendKeys(text);
        }
    }

    public static void click(WebElement element) {
        element.click();
    }

    public static void is_displayed(WebElement element) {
        element.isDisplayed();
    }

    public static void check_search_table_value(ArrayList<HashMap<String, WebElement>> elementArray, String column, String row, String value, String isVariable, HashMap<String, String> vMap) {
        String actual = elementArray.get(Integer.parseInt(row) - 1).get(column).getText();
        String expected="";
        if(Boolean.parseBoolean(isVariable)) {
            expected = vMap.get(value);
        } else {
            expected = value;
        }
        Assert.assertEquals(actual, expected, "checking search table value");
    }

    public static void check_search_table_cell_attribute_by_column(ArrayList<HashMap<String, WebElement>> elementArray, String column, String value, String count, String timeout, String retires, String tag, String attribute) throws InterruptedException {
        String allConnected = "green";
        int counter = 0;
        for(HashMap<String, WebElement> element : elementArray) {
            if(element.get(column).getTagName().equals(tag)) {
                if(!element.get(column).getAttribute(attribute).contains(value)) {
                    allConnected = element.get(column).getAttribute(attribute);
                } {
                    counter++;
                }
            }
        }
        if(Integer.parseInt(retires) > 0 && !allConnected.equals(value)) {
            Thread.sleep(Integer.parseInt(timeout) * 1000);
            check_search_table_cell_attribute_by_column(elementArray, column, value, count, timeout, String.valueOf(Integer.parseInt(retires) - 1), tag, attribute);
        }
        Assert.assertEquals(allConnected, value, column + ": validating search table cell attribute by column value");
        Assert.assertEquals(counter, Integer.parseInt(count), "validating the number of connected WDS");
    }

    public static void validate_table_value(WebElement element, String value) {
        Assert.assertEquals(element.getText(), value, "checking table text");
    }

    public static void click_options(WebElement element, String value) {
        element.findElement(By.xpath("ul/li/a[text()=' " + value + "']")).click();
    }

    public static void validate_parsed_text(By element, String sep, String property, String text) throws IOException {
        String textContent = findWebElement(element).getText();
        BufferedReader bufReader = new BufferedReader(new StringReader(textContent));
        String line = null;
        while( (line = bufReader.readLine()) != null ) {
            if(line.contains(sep)) {
                if(line.split(sep)[0].equals(property)) {
                    Assert.assertEquals(line.split(sep)[1].trim(), text, "checking the property of layout");
                }
            }
        }
    }

    public static void select_action(WebElement element, String action) {
        switch (action) {
            case "hover":
                BasePage.hover(element);
                break;
        }
    }

    public static void highlight(WebElement ele) {
        JavascriptExecutor js = (JavascriptExecutor) driver();
        js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", ele);
    }

    public static void click_field_row_text(WebElement element, String column, String value) {
        element.findElement(By.xpath("//div/label[text()='" + column + ":']/following-sibling::p/a[text()='" + value + "']")).click();
    }

    public static void check_attribute(WebElement element, String attribute, String value) {
        Assert.assertEquals(element.getAttribute(attribute), value, "checking attribute");
    }

    public static void validate_column_header(WebElement element, String columnIndex, String value) {
        Assert.assertEquals(Table.getColumnHeader(element, Integer.parseInt(columnIndex)), value, "validate column header");
    }

    public static void validate_title(String text) {
        Assert.assertEquals(driver().getTitle(), text, "Check browser title.");
    }

    public static void select_table_value(WebElement webElement, HashMap<String, String> vMap, String column, String row, String value, String isVariable) {
        WebElement element = Table.parseDynamicTable(webElement, Integer.parseInt(row)-1 , "select", column);
        if(Boolean.parseBoolean(isVariable)) {
            cucumber.pages.Building.select(element, vMap.get(value));
        } else {
            cucumber.pages.Building.select(element, value);
        }
    }

}
