package cucumber;

public class Wait {

    public static void min () {
        try {
            Thread.sleep(Long.parseLong(Config.get("explicit_short_wait")) * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void med () {
        try {
            Thread.sleep(Long.parseLong(Config.get("explicit_med_wait")) * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
