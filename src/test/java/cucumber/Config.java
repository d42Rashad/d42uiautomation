package cucumber;

import java.util.ResourceBundle;

public class Config {

    public static String env(String key) {
        return ResourceBundle.getBundle("config").getString(key);
    }

    public static String get(String key) {
        return System.getenv().get(key);
    }

}
