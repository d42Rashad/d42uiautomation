package cucumber.pages;

import cucumber.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.HashMap;

public class PowerCircuit extends BasePage {

    public static By addPowerCircuitButton() {
        return By.xpath("//a[@href='/admin/rackraj/powercircuit/add/']");
    }

    public static By numberTextbox() {
        return By.xpath("//input[@id='id_number']");
    }

    public static HashMap<String, By> getPageMap() {
        HashMap<String, By> pageMap = getBaseMap();
        pageMap.put("addPowerCircuitButton", addPowerCircuitButton());
        pageMap.put("numberTextbox", numberTextbox());
        return pageMap;
    }

    public static WebElement getElement(String obj) {
        return getElement(getPageMap().get(obj));
    }

}
