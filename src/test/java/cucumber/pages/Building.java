package cucumber.pages;

import cucumber.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.HashMap;

public class Building extends BasePage {

    public static By addBuildingButton() {
        return By.xpath("//a[@href='/admin/rackraj/building/add/']");
    }

    public static By nameTextbox() {
        return By.xpath("//input[@id='id_name']");
    }

    public static By addGroupButton() {
        return By.xpath("//a[text() = 'Add another Group ']");
    }

    public static By groupsForBuildingTable() {
        return By.xpath("//h2[text() = 'Groups For This Building']/following-sibling::table");
    }

    public static By roomsForBuildingTable() {
        return By.xpath("//h2[text() = 'Rooms']/following-sibling::table");
    }

    public static By groupFilterDropdown() {
        return By.xpath("//select[@id='filter-Group']");
    }

    public static HashMap<String, By> getPageMap() {
        HashMap<String, By> pageMap = getBaseMap();
        pageMap.put("addBuildingButton", addBuildingButton());
        pageMap.put("nameTextbox", nameTextbox());
        pageMap.put("addGroupButton", addGroupButton());
        pageMap.put("groupsForBuildingTable", groupsForBuildingTable());
        pageMap.put("groupFilterDropdown", groupFilterDropdown());
        pageMap.put("roomsForBuildingTable", roomsForBuildingTable());
        return pageMap;
    }

    public static WebElement getElement(String obj) {
        return getElement(getPageMap().get(obj));
    }

}
