package cucumber.pages;

import cucumber.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.HashMap;

public class Rack extends BasePage {

    public static By addRackButton() {
        return By.xpath("//a[@href='/admin/rackraj/rack/add/']");
    }

    public static By nameTextbox() {
        return By.xpath("//input[@id='id_name']");
    }

    public static By roomTextbox() {
        return By.xpath("//input[@id='id_room']");
    }

    public static By filterRoomDropdown() {
        return By.xpath("//select[@id='filter-Room']");
    }

    public static By filterRowDropdown() {
        return By.xpath("//select[@id='filter-Row']");
    }

    public static By filterTagsDropdown() {
        return By.xpath("//select[@id='filter-Tags']");
    }

    public static By editRackButton() {
        return By.xpath("//a[text()='Edit']");
    }

    public static By editModeButton() {
        return By.xpath("//button[@id='toggle_edit_mode']");
    }

    public static By roomFieldRows() {
        return By.xpath("//fieldset/div[@class='form-row field-row field-room']");
    }

    public static By devicesInRackTable() {
        return By.xpath("//h2[text() = 'Devices in this rack']/following-sibling::table");
    }

    public static By drawingRack() {
        return By.xpath("//div[@id='drawing_rack']");
    }

    public static WebElement rackSelection(String text) {
        return getElement("drawingRack").findElement(By.xpath("//*[local-name()='svg']/*[local-name()='text']/*[text()='" + text + "']"));
    }

    public static WebElement rackDevicePopupField(String text) {
        return BasePage.driver().findElement(By.xpath("//*[@id='context_tooltip']/div[@class='selection-info']/div[@class='popup_field']/span[text() = 'Name']/parent::div"));
    }

    public static HashMap<String, By> getPageMap() {
        HashMap<String, By> pageMap = getBaseMap();
        pageMap.put("nameTextbox", nameTextbox());
        pageMap.put("addRackButton", addRackButton());
        pageMap.put("roomTextbox", roomTextbox());
        pageMap.put("filterRoomDropdown", filterRoomDropdown());
        pageMap.put("filterTagsDropdown", filterTagsDropdown());
        pageMap.put("editRackButton", editRackButton());
        pageMap.put("roomFieldRows", roomFieldRows());
        pageMap.put("devicesInRackTable", devicesInRackTable());
        pageMap.put("filterRowDropdown", filterRowDropdown());
        pageMap.put("drawingRack", drawingRack());
        pageMap.put("editModeButton", editModeButton());
        return pageMap;
    }

    public static WebElement getElement(String obj) {
        return getElement(getPageMap().get(obj));
    }

}
