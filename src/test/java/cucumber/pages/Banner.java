package cucumber.pages;

import cucumber.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.HashMap;

public class Banner extends BasePage {

    public static By secretsLink() {
        return By.xpath("//a[@href='/admin/rowmgmt/password/']/span[text() = 'Secrets']");
    }

    public static By dataCenterLink() {
        return By.xpath("//span[text() = 'DataCenter']");
    }

    public static By buildingsLink() {
        return By.xpath("//a[@href = '/admin/rackraj/building/']");
    }

    public static By racksLink() {
        return By.xpath("//a[@href = '/admin/rackraj/rack/']");
    }

    public static By roomsLink() {
        return By.xpath("//a[@href = '/admin/rackraj/room/']");
    }

    public static By navigationMenu() {
        return By.xpath("//ul[@id ='navigation-menu']");
    }

    public static HashMap<String, By> getPageMap() {
        HashMap<String, By> pageMap = getBaseMap();
        pageMap.put("secretsLink", secretsLink());
        pageMap.put("dataCenterLink", dataCenterLink());
        pageMap.put("buildingsLink", buildingsLink());
        pageMap.put("roomsLink", roomsLink());
        pageMap.put("racksLink", racksLink());
        pageMap.put("navigationMenu", navigationMenu());
        return pageMap;
    }

    public static WebElement getElement(String obj) {
        return getElement(getPageMap().get(obj));
    }

}
