package cucumber.pages;

import cucumber.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Dashboard extends BasePage {

    public static By deviceStatsWidget() {
        return By.xpath("//div[text() = 'Device Statistics']");
    }

    public static By bldgStatsWidget() {
        return By.xpath("//div[text() = 'Building Statistics']");
    }

    public static By ipStatsWidget() {
        return By.xpath("//div[text() = 'IP Statistics']");
    }

    public static By monthlyDeviceAdditionWidget() {
        return By.xpath("//div[text() = 'Monthly Device Addition Trend']");
    }

    public static By monthlyIPAdditionWidget() {
        return By.xpath("//div[text() = 'Monthly IP Addition Trend']");
    }

    public static By myDashboard() {
        return By.xpath("//ul[@id='myDashboard']");
    }

    public static By instantSearchTextBox() {
        return By.xpath("//input[@id='s']");
    }

    public static HashMap<String, By> getPageMap() {
        HashMap<String, By> pageMap = getBaseMap();
        pageMap.put("instantSearchTextBox", instantSearchTextBox());
        pageMap.put("widget_devicestats", deviceStatsWidget());
        pageMap.put("widget_bldgstats", bldgStatsWidget());
        pageMap.put("widget_ipstats", ipStatsWidget());
        pageMap.put("widget_monthly_device_addition", monthlyDeviceAdditionWidget());
        pageMap.put("widget_monthly_IP_addition", monthlyIPAdditionWidget());
        pageMap.put("myDashboard", myDashboard());
        return pageMap;
    }

    public static List<String> getWidgetOrder() {
        List<WebElement> children = findWebElement(myDashboard()).findElements(By.xpath("li"));
        List<String> widgetOrder = new ArrayList<String>();
        for (WebElement child : children)
        {
            widgetOrder.add(child.getAttribute("id"));
        }
        return widgetOrder;
    }

    public static WebElement getElement(String obj) {
        return getElement(getPageMap().get(obj));
    }

}
