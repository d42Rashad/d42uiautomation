package cucumber.pages;

import cucumber.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.HashMap;

public class Room extends BasePage {

    public static By addRoomButton() {
        return By.xpath("//a[@href='/admin/rackraj/room/add/']");
    }

    public static By nameTextbox() {
        return By.xpath("//input[@id='id_name']");
    }

    public static By buildingTextbox() {
        return By.xpath("//input[@id='id_bldg']");
    }

    public static By roomLayoutGridText(String text) {
        return By.xpath("//*[@id='drawing']/*[local-name()='svg']/*[local-name()='text']/*[local-name()='tspan' and text()='" + text + "']");
    }

    public static By layoutPropertyText() {
        return By.xpath("//div[@id='drawing_rack']/following-sibling::div");
    }

    public static By byBuildingDropdown() {
        return By.xpath("//select[@id='filter-Building']");
    }

    public static By byTagsDropdown() {
        return By.xpath("//select[@id='filter-Tags']");
    }

    public static By racksForRoomsTable() {
        return By.xpath("//h2[text() = 'Racks']/following-sibling::table");
    }

    public static HashMap<String, By> getPageMap() {
        HashMap<String, By> pageMap = getBaseMap();
        pageMap.put("addRoomButton", addRoomButton());
        pageMap.put("nameTextbox", nameTextbox());
        pageMap.put("buildingTextbox", buildingTextbox());
        pageMap.put("byBuildingDropdown", byBuildingDropdown());
        pageMap.put("byTagsDropdown", byTagsDropdown());
        pageMap.put("racksForRoomsTable", racksForRoomsTable());
        return pageMap;
    }

    public static WebElement getElement(String obj) {
        return getElement(getPageMap().get(obj));
    }

}
