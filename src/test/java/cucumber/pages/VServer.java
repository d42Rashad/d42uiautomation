package cucumber.pages;

import cucumber.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.HashMap;

public class VServer extends BasePage {

    public static By jobStatusArea() {
        return By.xpath("//h2[text()='Job Status']");
    }

    public static By jobStatusContainer() {
        return By.xpath("//div[@id='job-status']");
    }

    public static HashMap<String, By> getPageMap() {
        HashMap<String, By> pageMap = getBaseMap();
        pageMap.put("jobStatusArea", jobStatusArea());
        pageMap.put("jobStatusContainer", jobStatusContainer());
        return pageMap;
    }

    public static WebElement getElement(String obj) {
        return getElement(getPageMap().get(obj));
    }

}
