package cucumber.pages;

import cucumber.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.HashMap;

public class Login extends BasePage {

    public static By usernameTextBox() {
        return By.xpath("//input[@id='id_username']");
    }

    public static By passwordTextBox() {
        return By.xpath("//input[@id='id_password']");
    }

    public static By loginButton() {
        return By.xpath("//button[@title='Log in']");
    }

    public static HashMap<String, By> getPageMap() {
        HashMap<String, By> pageMap = getBaseMap();
        pageMap.put("usernameTextBox", usernameTextBox());
        pageMap.put("passwordTextBox", passwordTextBox());
        pageMap.put("loginButton", loginButton());
        return pageMap;
    }

    public static void loginAsAdmin(String username, String password) {
        findWebElement(usernameTextBox()).sendKeys(username);
        findWebElement(passwordTextBox()).sendKeys(password);
        findWebElement(loginButton()).click();
    }

    public static WebElement getElement(String obj) {
        return getElement(getPageMap().get(obj));
    }

}
