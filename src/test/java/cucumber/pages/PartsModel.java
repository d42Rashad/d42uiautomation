package cucumber.pages;

import cucumber.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.HashMap;

public class PartsModel extends BasePage {

    public static By addPartsModelButton() {
        return By.xpath("//a[@href='/admin/rackraj/partmodel/add/']");
    }

    public static By nameTextbox() {
        return By.xpath("//input[@id='id_name']");
    }

    public static By partsModelType() {
        return By.xpath("//select[@id='id_type']");
    }

    public static HashMap<String, By> getPageMap() {
        HashMap<String, By> pageMap = getBaseMap();
        pageMap.put("addPartsModelButton", addPartsModelButton());
        pageMap.put("nameTextbox", nameTextbox());
        pageMap.put("partsModelType", partsModelType());
        return pageMap;
    }

    public static WebElement getElement(String obj) {
        return getElement(getPageMap().get(obj));
    }

}
