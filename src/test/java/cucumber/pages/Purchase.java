package cucumber.pages;

import cucumber.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.HashMap;

public class Purchase extends BasePage {

    public static By addPurchaseButton() {
        return By.xpath("//a[@href='/admin/rackraj/purchase/add/']");
    }

    public static By orderNameNoTextbox() {
        return By.xpath("//input[@id='id_order_no']");
    }

    public static HashMap<String, By> getPageMap() {
        HashMap<String, By> pageMap = getBaseMap();
        pageMap.put("addPurchaseButton", addPurchaseButton());
        pageMap.put("orderNameNoTextbox", orderNameNoTextbox());
        return pageMap;
    }

    public static WebElement getElement(String obj) {
        return getElement(getPageMap().get(obj));
    }

}
