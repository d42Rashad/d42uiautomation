package cucumber.pages;

import cucumber.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.HashMap;

public class CustDept extends BasePage {

    public static By addCustDeptButton() {
        return By.xpath("//a[@href='/admin/rackraj/customer/add/']");
    }

    public static By nameTextbox() {
        return By.xpath("//input[@id='id_name']");
    }

    public static By typeDropdown() {
        return By.xpath("//select[@id='id_type']");
    }

    public static HashMap<String, By> getPageMap() {
        HashMap<String, By> pageMap = getBaseMap();
        pageMap.put("addCustDeptButton", addCustDeptButton());
        pageMap.put("nameTextbox", nameTextbox());
        pageMap.put("typeDropdown", typeDropdown());
        return pageMap;
    }

    public static WebElement getElement(String obj) {
        return getElement(getPageMap().get(obj));
    }

}
