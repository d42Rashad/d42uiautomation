package cucumber.pages;

import cucumber.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.HashMap;

public class PowerUnitModel extends BasePage {

    public static By addPowerUnitModelButton() {
        return By.xpath("//a[@href='/admin/rackraj/pdu_model/add/']");
    }

    public static By nameTextbox() {
        return By.xpath("//input[@id='id_name']");
    }

    public static HashMap<String, By> getPageMap() {
        HashMap<String, By> pageMap = getBaseMap();
        pageMap.put("addPowerUnitModelButton", addPowerUnitModelButton());
        pageMap.put("nameTextbox", nameTextbox());
        return pageMap;
    }

    public static WebElement getElement(String obj) {
        return getElement(getPageMap().get(obj));
    }

}
