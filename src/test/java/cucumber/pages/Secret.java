package cucumber.pages;

import cucumber.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.HashMap;

public class Secret extends BasePage {

    public static By addPasswordButton() {
        return By.xpath("//a[@href='/admin/rowmgmt/password/add/']");
    }

    public static By usernameTextbox() {
        return By.xpath("//input[@id='id_username']");
    }

    public static By labelTextbox() {
        return By.xpath("//input[@id='id_label']");
    }

    public static By daysToExpireTextbox() {
        return By.xpath("//input[@id='id_days_to_expire']");
    }

    public static By passwordTextarea() {
        return By.xpath("//textarea[@id='id_password']");
    }

    public static By secretTextPeekToggle() {
        return By.xpath("//span[@class='secret-text-peek']");
    }

    public static By storageDropdown() {
        return By.xpath("//select[@id='id_storage']");
    }

    public static By passwordContainer() {
        return By.xpath("//textarea[@id='id_password']/parent::span");
    }

    public static By editSecretButton() {
        return By.xpath("//a[text()='Edit']");
    }

    public static HashMap<String, By> getPageMap() {
        HashMap<String, By> pageMap = getBaseMap();
        pageMap.put("addPasswordButton", addPasswordButton());
        pageMap.put("usernameTextbox", usernameTextbox());
        pageMap.put("labelTextbox", labelTextbox());
        pageMap.put("daysToExpireTextbox", daysToExpireTextbox());
        pageMap.put("passwordTextarea", passwordTextarea());
        pageMap.put("secretTextPeekToggle", secretTextPeekToggle());
        pageMap.put("storageDropdown", storageDropdown());
        pageMap.put("passwordContainer", passwordContainer());
        pageMap.put("editSecretButton", editSecretButton());
        return pageMap;
    }

    public static WebElement getElement(String obj) {
        return getElement(getPageMap().get(obj));
    }

}
