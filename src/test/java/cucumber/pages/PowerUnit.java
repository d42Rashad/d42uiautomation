package cucumber.pages;

import cucumber.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.HashMap;

public class PowerUnit extends BasePage {

    public static By addPowerUnitButton() {
        return By.xpath("//a[@href='/admin/rackraj/pdu/add/']");
    }

    public static By nameTextbox() {
        return By.xpath("//input[@id='id_name']");
    }

    public static HashMap<String, By> getPageMap() {
        HashMap<String, By> pageMap = getBaseMap();
        pageMap.put("addPowerUnitButton", addPowerUnitButton());
        pageMap.put("nameTextbox", nameTextbox());
        return pageMap;
    }

    public static WebElement getElement(String obj) {
        return getElement(getPageMap().get(obj));
    }
}
