package cucumber.pages;

import cucumber.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.HashMap;

public class Part extends BasePage {

    public static By addPartButton() {
        return By.xpath("//a[@href='/admin/rackraj/part/add/']");
    }

    public static By partModelTextbox() {
        return By.xpath("//input[@id='id_PartModel']");
    }

    public static HashMap<String, By> getPageMap() {
        HashMap<String, By> pageMap = getBaseMap();
        pageMap.put("addPartButton", addPartButton());
        pageMap.put("partModelTextbox", partModelTextbox());
        return pageMap;
    }

    public static WebElement getElement(String obj) {
        return getElement(getPageMap().get(obj));
    }

}
