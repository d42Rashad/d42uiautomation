package cucumber.pages;

import cucumber.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.HashMap;

public class TelcoCircuit extends BasePage {

    public static By addTelcoCircuitButton() {
        return By.xpath("//a[@href='/admin/rackraj/circuit/add/']");
    }

    public static By circuitIDTextbox() {
        return By.xpath("//input[@id='id_circuit_id']");
    }

    public static By circuitTypeDropdown() {
        return By.xpath("//select[@id='id_type']");
    }

    public static HashMap<String, By> getPageMap() {
        HashMap<String, By> pageMap = getBaseMap();
        pageMap.put("addTelcoCircuitButton", addTelcoCircuitButton());
        pageMap.put("circuitIDTextbox", circuitIDTextbox());
        pageMap.put("circuitTypeDropdown", circuitTypeDropdown());
        return pageMap;
    }

    public static WebElement getElement(String obj) {
        return getElement(getPageMap().get(obj));
    }

}
