package cucumber;

import java.sql.*;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;

public class Database {

    private final String url = "jdbc:postgresql://" + Config.get("db_ip") + ":" + Config.get("db_port") + "/vserver";
    private final String user = "postgres";
    private final String password = "password";
    public view_jobscore_v1_performance view;

    public Database() {
        view = new view_jobscore_v1_performance();
    }


    public Connection connect() throws SQLException {
        return DriverManager.getConnection(url, user, password);
    }

    public long insertRow(view_jobscore_v1_performance job) {
        String SQL = "INSERT INTO public.view_jobscore_v1_performance(task_type, basic_sendrc, basic_runstart, basic_runend, basic_queueend, detailed_sendrc, detailed_runstart, detailed_runend, detailed_queueend, job_name, d42_build_no, port_check_failed, total_attempted, auth_failed, discovery_exception, success, rc_name, ip_address) "
                + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        long id = 0;

        try (Connection conn = connect();
             PreparedStatement pstmt = conn.prepareStatement(SQL,
                     Statement.RETURN_GENERATED_KEYS)) {

            pstmt.setString(1,job.task_type);
            pstmt.setTimestamp(2,job.basic_sendrc);
            pstmt.setTimestamp(3,job.basic_runstart);
            pstmt.setTimestamp(4,job.basic_runend);
            pstmt.setTimestamp(5,job.basic_queueend);
            pstmt.setTimestamp(6,job.detailed_sendrc);
            pstmt.setTimestamp(7,job.detailed_runstart);
            pstmt.setTimestamp(8,job.detailed_runend);
            pstmt.setTimestamp(9,job.detailed_queueend);
            pstmt.setString(10,job.job_name);
            pstmt.setString(11,job.d42_build_no);
            pstmt.setInt(12,job.port_check_failed);
            pstmt.setInt(13,job.total_attempted);
            pstmt.setInt(14,job.auth_failed);
            pstmt.setInt(15,job.discovery_exception);
            pstmt.setInt(16,job.success);
            pstmt.setString(17,job.rc_name);
            pstmt.setString(18,job.ip_address);
            int affectedRows = pstmt.executeUpdate();
            if (affectedRows > 0) {
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                }
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return id;
    }

    public class view_jobscore_v1_performance {
        public String task_type;
        public Timestamp basic_sendrc;
        public Timestamp basic_runstart;
        public Timestamp basic_runend;
        public Timestamp basic_queueend;
        public Timestamp detailed_sendrc;
        public Timestamp detailed_runstart;
        public Timestamp detailed_runend;
        public Timestamp detailed_queueend;
        public String job_name;
        public String d42_build_no;
        public int port_check_failed;
        public int total_attempted;
        public int auth_failed;
        public int discovery_exception;
        public int success;
        public String rc_name;
        public String ip_address;

    }

    public Instant getInstant(String timestamp) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        TemporalAccessor temporalAccessor = formatter.parse(timestamp);
        LocalDateTime localDateTime = LocalDateTime.from(temporalAccessor);
        ZonedDateTime zonedDateTime = ZonedDateTime.of(localDateTime, ZoneId.systemDefault());
        return Instant.from(zonedDateTime);
    }

}
