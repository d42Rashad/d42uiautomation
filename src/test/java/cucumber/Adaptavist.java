package cucumber;

public class Adaptavist {

    public AdaptavistPayload apl;

    public Adaptavist(String testCaseKey, String status) {
        apl = new AdaptavistPayload();
        apl.projectKey = Config.get("adaptavist_project_key");
        apl.testCaseKey = testCaseKey;
        apl.testCycleKey = Config.get("adaptavist_test_cycle_key");
        apl.statusName = status;
        apl.comment = "<a href=\"" + Config.get("adaptavist_comment_template") + "\">Click here for complete report...</a>";
    }

    public class AdaptavistPayload {
        String projectKey;
        String testCaseKey;
        String testCycleKey;
        String statusName;
        String comment;
    }

}
