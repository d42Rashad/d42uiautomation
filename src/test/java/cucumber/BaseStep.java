package cucumber;

import io.cucumber.java.en.Then;

import java.io.IOException;
import java.time.Instant;

public interface BaseStep {

    void type(String obj, String text, String isVariable);

    void click(String obj);

    void is_displayed(String obj);

    void select(String obj, String text);

    void check_attribute(String obj, String attribute, String value);

    void check_search_table_value(String obj, String column, String row, String value, String isVariable);

    void click_search_table_value(String obj, String column, String row);

    void validate_text(String obj, String value);

    void validate_table_column_header(String obj, String columnIndex, String value);

    void set_variable(String key, String val, String timestamp, String random);

    void hover(String obj);

    void validate_title(String text);

    void select_table_value(String obj, String column, String row, String value, String isVariable);

    void validate_table_value(String obj, String column, String row, String value);

    void clear(String obj);

    void validate_table_count(String obj, String text);

    void validate_table_column_value(String obj, String column, String value);

    void click_field_row_text(String obj, String column, String value);

    void click_options(String obj, String value);

    void gridLayout_actions(String action, String value);

    void validate_room_layout_property(String property, String text) throws IOException;

    void validate_text_contains(String obj, String text);

    void validate_all_table_column_values(String obj, String column, String value);

    void click_results_table_value_by_key(String obj, String searchColumn, String clickColumn, String value);

}