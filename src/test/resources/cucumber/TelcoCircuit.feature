Feature: Telco Circuits

  @t1656 @smoke
  Scenario: [D42-T1656] Verify the creation of a new Telco Circuit
    Given Logged in as admin
    Then TelcoCircuit - set-variable - key: "t1656telcoCircuitName" value: "t1656" timestamp: "true" random: "true"
    Then Banner - navigation-left-menu - path: "DataCenter > Telco Circuits"
    Then TelcoCircuit - click - obj: "addTelcoCircuitButton"
    Then TelcoCircuit - validate-text - obj: "pageHeader" value: "Add Telco Circuit"
    Then TelcoCircuit - validate-title - text: "Add Telco Circuit | Device42"
    Then TelcoCircuit - type - obj: "circuitIDTextbox" text: "t1656telcoCircuitName" isVariable: "true"
    Then TelcoCircuit - select - obj: "circuitTypeDropdown" value: "internet"
    Then TelcoCircuit - click - obj: "saveButton"
    Then TelcoCircuit - type - obj: "searchTextbox" text: "t1656telcoCircuitName" isVariable: "true"
    Then TelcoCircuit - click - obj: "searchButton"
    Then TelcoCircuit - check-search-table-value - obj: "result_listTable" column: "Circuit ID" row: "1" value: "t1656telcoCircuitName" variable: "true"


