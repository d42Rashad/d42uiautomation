Feature: Hardware Model

  @t863 @smoke
  Scenario: [D42-T863] Verify the creation of a new Hardware
    Given Logged in as admin
    Then HardwareModel - set-variable - key: "t863hardwareModelName" value: "t863" timestamp: "true" random: "true"
    Then Banner - navigation-left-menu - path: "DataCenter > Hardware Models"
    Then HardwareModel - click - obj: "addHardwareModelButton"
    Then HardwareModel - validate-text - obj: "pageHeader" value: "Add Device Hardware Model"
    Then HardwareModel - validate-title - text: "Add Device Hardware Model | Device42"
    Then HardwareModel - type - obj: "nameTextbox" text: "t863hardwareModelName" isVariable: "true"
    Then HardwareModel - click - obj: "saveButton"
    Then HardwareModel - type - obj: "searchTextbox" text: "t863hardwareModelName" isVariable: "true"
    Then HardwareModel - click - obj: "searchButton"
    Then HardwareModel - check-search-table-value - obj: "result_listTable" column: "Name" row: "1" value: "t863hardwareModelName" variable: "true"