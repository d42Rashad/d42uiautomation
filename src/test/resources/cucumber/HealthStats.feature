Feature: Health Stats

  @t873 @healthStatus
  Scenario: [D42-T873] Validate the status of a D42 installation
    Given HealthStats - status-check
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > Remote Collectors"
    Then HealthStats - validate-attribute - attribute: "version" value: ""
    Then HealthStats - validate-rc-count - value: "5"
    Then HealthStats - validate-rc-status - timeout: "60" retries: "10"
    Then RemoteCollector - check-search-table-cell-attribute-by-column - column: "WDS" value: "green" count: "5" timeout: "60" retries: "10"

  @rc1
  Scenario: [D42-TXXX] RC Check
    Given HealthStats - status-check
    Then HealthStats - validate-rc-status - value: "rc1" timeout: "60" retries: "10"

  @rc2
  Scenario: [D42-TXXX] RC Check
    Given HealthStats - status-check
    Then HealthStats - validate-rc-status - value: "rc2" timeout: "60" retries: "10"

  @rc3
  Scenario: [D42-TXXX] RC Check
    Given HealthStats - status-check
    Then HealthStats - validate-rc-status - value: "rc3" timeout: "60" retries: "10"

  @rc4
  Scenario: [D42-TXXX] RC Check
    Given HealthStats - status-check
    Then HealthStats - validate-rc-status - value: "rc4" timeout: "60" retries: "10"

  @rc5
  Scenario: [D42-TXXX] RC Check
    Given HealthStats - status-check
    Then HealthStats - validate-rc-status - value: "rc5" timeout: "60" retries: "10"

  @wds1
  Scenario: [D42-TYYY] Validate the status of a D42 installation
    Given HealthStats - status-check
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > Remote Collectors"
    Then RemoteCollector - validate-wds-status - rc: "rc1" timeout: "60" retries: "10"

  @wds2
  Scenario: [D42-TYYY] Validate the status of a D42 installation
    Given HealthStats - status-check
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > Remote Collectors"
    Then RemoteCollector - validate-wds-status - rc: "rc2" timeout: "60" retries: "10"

  @wds3
  Scenario: [D42-TYYY] Validate the status of a D42 installation
    Given HealthStats - status-check
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > Remote Collectors"
    Then RemoteCollector - validate-wds-status - rc: "rc3" timeout: "60" retries: "10"


  @wds4
  Scenario: [D42-TYYY] Validate the status of a D42 installation
    Given HealthStats - status-check
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > Remote Collectors"
    Then RemoteCollector - validate-wds-status - rc: "rc4" timeout: "60" retries: "10"

  @wds5
  Scenario: [D42-TYYY] Validate the status of a D42 installation
    Given HealthStats - status-check
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > Remote Collectors"
    Then RemoteCollector - validate-wds-status - rc: "rc5" timeout: "60" retries: "10"
