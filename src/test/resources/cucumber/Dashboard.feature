Feature: Dashboard

  @t771 @smoke
  Scenario: [D42-T771] Verify that there are 5 widgets on Home page
    Given Logged in as admin
    Then Dashboard - isDisplayed - obj: "widget_devicestats"
    Then Dashboard - isDisplayed - obj: "widget_bldgstats"
    Then Dashboard - isDisplayed - obj: "widget_ipstats"
    Then Dashboard - isDisplayed - obj: "widget_monthly_device_addition"
    Then Dashboard - isDisplayed - obj: "widget_monthly_IP_addition"

  @t772
  Scenario: [D42-T772] Verify that user is able to relocate the widget
    Given Logged in as admin
    Then Dashboard - Rearrange widgets