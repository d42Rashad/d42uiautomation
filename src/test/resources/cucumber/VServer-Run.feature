@vServer
Feature: VServer

  @25kPerformanceRun
  Scenario Outline: Execute 25k Performance Run
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: <jobName> isVariable: "false"
    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "commands" row: "1"
    Examples:
      | jobName |
      | "87Win_Part1" |
      | "87Win_Part2" |
      | "88Win_Part1" |
      | "88Win_Part2" |
      | "89Win_Part1" |
      | "89Win_Part2" |
      | "89Win_Part3" |
      | "89Win_Part4" |
      | "89Win_Part5" |
      | "87Nix_Part1" |
      | "87Nix_Part2" |
      | "88Nix_Part1" |
      | "88Nix_Part2" |
      | "89Nix_Part1" |
      | "89Nix_Part2" |
      | "89Nix_Part3" |
      | "89Nix_Part4" |
      | "89Nix_Part5" |

  @SanityPerformanceRun
  Scenario Outline: Execute Sanity Performance Run
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: <jobName> isVariable: "false"
    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "commands" row: "1"
    Examples:
      | jobName |
      | "88Win_Part1" |
      | "88NixPart2" |

  @87Win_Part1 #TODO: 1. Use left/right for set-variable-from-object-text when referencing the left or right table on VServer page instead of index.
  Scenario: Execute 87Win_Part1
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: "87Win_Part1" isVariable: "false"
    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "commands" row: "1"
    Then VServer - wait-for-job-complete - view: "view_jobscore_v1_performance" key: "vServer - 109" jobName: "87Win_Part1" timeout "60" retries: "3000"
    Then VServer - click - obj: "searchButton"

  @87Win_Part2 #TODO: 1. Use left/right for set-variable-from-object-text when referencing the left or right table on VServer page instead of index.
  Scenario: Execute 87Win_Part2
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: "87Win_Part2" isVariable: "false"
    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "commands" row: "1"
    Then VServer - wait-for-job-complete - view: "view_jobscore_v1_performance" key: "vServer - 95" jobName: "87Win_Part2" timeout "60" retries: "3000"
    Then VServer - click - obj: "searchButton"

  @88Win_Part1 #TODO: 1. Use left/right for set-variable-from-object-text when referencing the left or right table on VServer page instead of index.
  Scenario: Execute 88Win_Part1
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: "88Win_Part1" isVariable: "false"
    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "commands" row: "1"
    Then VServer - wait-for-job-complete - view: "view_jobscore_v1_performance" key: "vServer - 111" jobName: "88Win_Part1" timeout "60" retries: "3000"
    Then VServer - click - obj: "searchButton"

  @88Win_Part2 #TODO: 1. Use left/right for set-variable-from-object-text when referencing the left or right table on VServer page instead of index.
  Scenario: Execute 88Win_Part2
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: "88Win_Part2" isVariable: "false"
    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "commands" row: "1"
    Then VServer - wait-for-job-complete - view: "view_jobscore_v1_performance" key: "vServer - 96" jobName: "88Win_Part2" timeout "60" retries: "3000"
    Then VServer - click - obj: "searchButton"

  @89Win_Part1 #TODO: 1. Use left/right for set-variable-from-object-text when referencing the left or right table on VServer page instead of index.
  Scenario: Execute 89Win_Part1
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: "89Win_Part1" isVariable: "false"
    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "commands" row: "1"
    Then VServer - wait-for-job-complete - view: "view_jobscore_v1_performance" key: "vServer - 113" jobName: "89Win_Part1" timeout "60" retries: "3000"
    Then VServer - click - obj: "searchButton"

  @89Win_Part2 #TODO: 1. Use left/right for set-variable-from-object-text when referencing the left or right table on VServer page instead of index.
  Scenario: Execute 89Win_Part2
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: "89Win_Part2" isVariable: "false"
    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "commands" row: "1"
    Then VServer - wait-for-job-complete - view: "view_jobscore_v1_performance" key: "vServer - 115" jobName: "89Win_Part2" timeout "60" retries: "3000"
    Then VServer - click - obj: "searchButton"

  @89Win_Part3 #TODO: 1. Use left/right for set-variable-from-object-text when referencing the left or right table on VServer page instead of index.
  Scenario: Execute 89Win_Part3
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: "89Win_Part3" isVariable: "false"
    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "commands" row: "1"
    Then VServer - wait-for-job-complete - view: "view_jobscore_v1_performance" key: "vServer - 117" jobName: "89Win_Part3" timeout "60" retries: "3000"
    Then VServer - click - obj: "searchButton"

  @89Win_Part4 #TODO: 1. Use left/right for set-variable-from-object-text when referencing the left or right table on VServer page instead of index.
  Scenario: Execute 89Win_Part4
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: "89Win_Part4" isVariable: "false"
    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "commands" row: "1"
    Then VServer - wait-for-job-complete - view: "view_jobscore_v1_performance" key: "vServer - 119" jobName: "89Win_Part4" timeout "60" retries: "3000"
    Then VServer - click - obj: "searchButton"

  @89Win_Part5 #TODO: 1. Use left/right for set-variable-from-object-text when referencing the left or right table on VServer page instead of index.
  Scenario: Execute 89Win_Part5
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: "89Win_Part5" isVariable: "false"
    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "commands" row: "1"
    Then VServer - wait-for-job-complete - view: "view_jobscore_v1_performance" key: "vServer - 94" jobName: "89Win_Part5" timeout "60" retries: "3000"
    Then VServer - click - obj: "searchButton"

  @87Nix_Part1 #TODO: 1. Use left/right for set-variable-from-object-text when referencing the left or right table on VServer page instead of index.
  Scenario: Execute 87Nix_Part1
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: "87Nix_Part1" isVariable: "false"
    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "commands" row: "1"
    Then VServer - wait-for-job-complete - view: "view_jobscore_v1_performance" key: "vServer - 110" jobName: "87Nix_Part1" timeout "60" retries: "3000"
    Then VServer - click - obj: "searchButton"

  @87Nix_Part2 #TODO: 1. Use left/right for set-variable-from-object-text when referencing the left or right table on VServer page instead of index.
  Scenario: Execute 87Nix_Part2
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: "87Nix_Part2" isVariable: "false"
    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "commands" row: "1"
    Then VServer - wait-for-job-complete - view: "view_jobscore_v1_performance" key: "vServer - 86" jobName: "87Nix_Part2" timeout "60" retries: "3000"
    Then VServer - click - obj: "searchButton"

  @88Nix_Part1 #TODO: 1. Use left/right for set-variable-from-object-text when referencing the left or right table on VServer page instead of index.
  Scenario: Execute 88Nix_Part1
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: "88Nix_Part1" isVariable: "false"
    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "commands" row: "1"
    Then VServer - wait-for-job-complete - view: "view_jobscore_v1_performance" key: "vServer - 95" jobName: "88Nix_Part1" timeout "60" retries: "3000"
    Then VServer - click - obj: "searchButton"

  @88Nix_Part2 #TODO: 1. Use left/right for set-variable-from-object-text when referencing the left or right table on VServer page instead of index.
  Scenario: Execute 88Nix_Part2
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: "88Nix_Part2" isVariable: "false"
    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "commands" row: "1"
    Then VServer - wait-for-job-complete - view: "view_jobscore_v1_performance" key: "vServer - 87" jobName: "88Nix_Part2" timeout "60" retries: "3000"
    Then VServer - click - obj: "searchButton"

  @89Nix_Part1 #TODO: 1. Use left/right for set-variable-from-object-text when referencing the left or right table on VServer page instead of index.
  Scenario: Execute 89Nix_Part1
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: "89Nix_Part1" isVariable: "false"
    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "commands" row: "1"
    Then VServer - wait-for-job-complete - view: "view_jobscore_v1_performance" key: "vServer - 119" jobName: "89Nix_Part1" timeout "60" retries: "3000"
    Then VServer - click - obj: "searchButton"

  @89Nix_Part2 #TODO: 1. Use left/right for set-variable-from-object-text when referencing the left or right table on VServer page instead of index.
  Scenario: Execute 89Nix_Part2
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: "89Nix_Part2" isVariable: "false"
    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "commands" row: "1"
    Then VServer - wait-for-job-complete - view: "view_jobscore_v1_performance" key: "vServer - 114" jobName: "89Nix_Part2" timeout "60" retries: "3000"
    Then VServer - click - obj: "searchButton"

  @89Nix_Part3 #TODO: 1. Use left/right for set-variable-from-object-text when referencing the left or right table on VServer page instead of index.
  Scenario: Execute 89Nix_Part3
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: "89Nix_Part3" isVariable: "false"
    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "commands" row: "1"
    Then VServer - wait-for-job-complete - view: "view_jobscore_v1_performance" key: "vServer - 118" jobName: "89Nix_Part3" timeout "60" retries: "3000"
    Then VServer - click - obj: "searchButton"

  @89Nix_Part4 #TODO: 1. Use left/right for set-variable-from-object-text when referencing the left or right table o    n VServer page instead of index.
  Scenario: Execute 89Nix_Part4
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: "89Nix_Part4" isVariable: "false"
    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "commands" row: "1"
    Then VServer - wait-for-job-complete - view: "view_jobscore_v1_performance" key: "vServer - 120" jobName: "89Nix_Part4" timeout "60" retries: "3000"
    Then VServer - click - obj: "searchButton"

  @89Nix_Part5 #TODO: 1. Use left/right for set-variable-from-object-text when referencing the left or right table on VServer page instead of index.
  Scenario: Execute 89Nix_Part5
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: "89Nix_Part5" isVariable: "false"
    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "commands" row: "1"
    Then VServer - wait-for-job-complete - view: "view_jobscore_v1_performance" key: "vServer - 88" jobName: "89Nix_Part5" timeout "60" retries: "3000"
    Then VServer - click - obj: "searchButton"

