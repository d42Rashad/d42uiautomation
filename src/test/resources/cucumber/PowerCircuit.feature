Feature: Power Circuits

  @t1661 @smoke
  Scenario: [D42-T1661] Verify the creation of a new Power Circuit
    Given Logged in as admin
    Then PowerCircuit - set-variable - key: "t1661powerCircuitName" value: "t1661" timestamp: "true" random: "true"
    Then Banner - navigation-left-menu - path: "DataCenter > Power Circuits"
    Then PowerCircuit - click - obj: "addPowerCircuitButton"
    Then PowerCircuit - validate-text - obj: "pageHeader" value: "Add Power Circuit"
    Then PowerCircuit - validate-title - text: "Add Power Circuit | Device42"
    Then PowerCircuit - type - obj: "numberTextbox" text: "t1661powerCircuitName" isVariable: "true"
    Then PowerCircuit - click - obj: "saveButton"
    Then PowerCircuit - type - obj: "searchTextbox" text: "t1661powerCircuitName" isVariable: "true"
    Then PowerCircuit - click - obj: "searchButton"
    Then PowerCircuit - check-search-table-value - obj: "result_listTable" column: "Number" row: "1" value: "t1661powerCircuitName" variable: "true"