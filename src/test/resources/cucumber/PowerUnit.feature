Feature: Power Unit

  @t843 @smoke
  Scenario: [D42-T843] Verify the creation of a new Power Unit of PDU type
    Given Logged in as admin
    Then PowerUnit - set-variable - key: "t843pduName" value: "t843" timestamp: "true" random: "true"
    Then Banner - navigation-left-menu - path: "DataCenter > Power Units > Power Units"
    Then PowerUnit - click - obj: "addPowerUnitButton"
    Then PowerUnit - validate-text - obj: "pageHeader" value: "Add Power Unit"
    Then PowerUnit - validate-title - text: "Add Power Unit | Device42"
    Then PowerUnit - type - obj: "nameTextbox" text: "t843pduName" isVariable: "true"
    Then PowerUnit - click - obj: "saveButton"
    Then PowerUnit - type - obj: "searchTextbox" text: "t843pduName" isVariable: "true"
    Then PowerUnit - click - obj: "searchButton"
    Then PowerUnit - check-search-table-value - obj: "result_listTable" column: "Name" row: "1" value: "t843pduName" variable: "true"
