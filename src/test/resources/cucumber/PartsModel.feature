Feature: Parts Model

  @t868 @smoke
  Scenario: [D42-T868] Verify the creation of a new Parts Model
    Given Logged in as admin
    Then PartsModel - set-variable - key: "t868partsModelName" value: "t868" timestamp: "true" random: "true"
    Then Banner - navigation-left-menu - path: "DataCenter > Parts > Model List"
    Then PartsModel - click - obj: "addPartsModelButton"
    Then PartsModel - validate-text - obj: "pageHeader" value: "Add Parts Model"
    Then PartsModel - validate-title - text: "Add Parts Model | Device42"
    Then PartsModel - select - obj: "partsModelType" value: "CPU"
    Then PartsModel - type - obj: "nameTextbox" text: "t868partsModelName" isVariable: "true"
    Then PartsModel - click - obj: "saveButton"
    Then PartsModel - type - obj: "searchTextbox" text: "t868partsModelName" isVariable: "true"
    Then PartsModel - click - obj: "searchButton"
    Then PartsModel - check-search-table-value - obj: "result_listTable" column: "Name" row: "1" value: "t868partsModelName" variable: "true"