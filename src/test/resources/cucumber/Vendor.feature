Feature: Vendor

  @t858 @smoke
  Scenario: [D42-T858] Verify the creation of a new Vendor
    Given Logged in as admin
    Then Vendor - set-variable - key: "t858vendorName" value: "t851" timestamp: "true" random: "true"
    Then Banner - navigation-left-menu - path: "DataCenter > Vendors"
    Then Vendor - click - obj: "addVendorButton"
    Then Vendor - validate-text - obj: "pageHeader" value: "Add Vendor"
    Then Vendor - validate-title - text: "Add Vendor | Device42"
    Then Vendor - type - obj: "nameTextbox" text: "t858vendorName" isVariable: "true"
    Then Vendor - click - obj: "saveButton"
    Then Vendor - type - obj: "searchTextbox" text: "t858vendorName" isVariable: "true"
    Then Vendor - click - obj: "searchButton"
    Then Vendor - check-search-table-value - obj: "result_listTable" column: "Name" row: "1" value: "t858vendorName" variable: "true"

