Feature: Parts

  @t873 @smoke @weak
  Scenario: [D42-T873] Verify the creation of a new Part
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "DataCenter > Parts > Parts List"
    Then Part - click - obj: "addPartButton"
    Then Part - validate-text - obj: "pageHeader" value: "Add Part"
    Then Part - validate-title - text: "Add Part | Device42"
    Then Part - type - obj: "partModelTextbox" text: "1" isVariable: "false"
    Then Part - click - obj: "saveButton"
    Then Part - check-search-table-value - obj: "result_listTable" column: "Part Model" row: "1" value: "CPU CPU, Pro" variable: "false"
