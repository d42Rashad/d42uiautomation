Feature: Room

  @t811 @smoke
  Scenario: [D42-T811] Verify the creation of a room
    Given Logged in as admin
    Then Room - set-variable - key: "t811roomName" value: "t811" timestamp: "true" random: "true"
    Then Banner - hover - obj: "dataCenterLink"
    Then Banner - click - obj: "roomsLink"
    Then Room - click - obj: "addRoomButton"
    Then Room - validate-text - obj: "pageHeader" value: "Add Room"
    Then Banner - validate-title - text: "Add Room | Device42"
    Then Room - type - obj: "nameTextbox" text: "t811roomName" isVariable: "true"
    Then Room - type - obj: "buildingTextbox" text: "1" isVariable: "false"
    Then Room - click - obj: "saveButton"
    Then Room - type - obj: "searchTextbox" text: "t811roomName" isVariable: "true"
    Then Room - click - obj: "searchButton"
    Then Room - check-search-table-value - obj: "result_listTable" column: "Name" row: "1" value: "t811roomName" variable: "true"

  @t816
  Scenario: [D42-T816] Verify room list page - choose a couple filters
    Given Logged in as admin
    Then Banner - hover - obj: "dataCenterLink"
    Then Banner - click - obj: "roomsLink"
    Then Room - select - obj: "byBuildingDropdown" value: "NHC"
    Then Room - select - obj: "byTagsDropdown" value: "TestTag"
    Then Room - check-search-table-value - obj: "result_listTable" column: "Name" row: "1" value: "IT Lab New Haven" variable: "false"

  @t817
  Scenario: [D42-T817] Verify that there are some racks in a room
    Given Logged in as admin
    Then Banner - hover - obj: "dataCenterLink"
    Then Banner - click - obj: "roomsLink"
    Then Room - type - obj: "searchTextbox" text: "IDF2" isVariable: "false"
    Then Room - click - obj: "searchButton"
    Then Room - click-search-table-value - obj: "result_listTable" column: "Name" row: "1"
    Then Room - validate-table-value - obj: "racksForRoomsTable" column: "field-name" row: "1" value: "NHCTIDF2R1"
