Feature: Customers or Departments

  @t878 @smoke
  Scenario: [D42-T878] Verify the creation of a new Customer
    Given Logged in as admin
    Then CustDept - set-variable - key: "t878CustDeptName" value: "t878" timestamp: "true" random: "true"
    Then Banner - navigation-left-menu - path: "DataCenter > Customers/Departments"
    Then CustDept - click - obj: "addCustDeptButton"
    Then PowerUnit - validate-text - obj: "pageHeader" value: "Add Customer or Department"
    Then PowerUnit - validate-title - text: "Add Customer or Department | Device42"
    Then PowerUnit - type - obj: "nameTextbox" text: "t878CustDeptName" isVariable: "true"
    Then CustDept - select - obj: "typeDropdown" value: "Customer"
    Then PowerUnit - click - obj: "saveButton"
    Then PowerUnit - type - obj: "searchTextbox" text: "t878CustDeptName" isVariable: "true"
    Then PowerUnit - click - obj: "searchButton"
    Then PowerUnit - check-search-table-value - obj: "result_listTable" column: "Name" row: "1" value: "t878CustDeptName" variable: "true"

  @t1647 @smoke
  Scenario: [D42-T1647] Verify the creation of a new Department
    Given Logged in as admin
    Then CustDept - set-variable - key: "t1647CustDeptName" value: "t1647" timestamp: "true" random: "true"
    Then Banner - navigation-left-menu - path: "DataCenter > Customers/Departments"
    Then CustDept - click - obj: "addCustDeptButton"
    Then PowerUnit - validate-text - obj: "pageHeader" value: "Add Customer or Department"
    Then PowerUnit - validate-title - text: "Add Customer or Department | Device42"
    Then PowerUnit - type - obj: "nameTextbox" text: "t1647CustDeptName" isVariable: "true"
    Then CustDept - select - obj: "typeDropdown" value: "Department"
    Then PowerUnit - click - obj: "saveButton"
    Then PowerUnit - type - obj: "searchTextbox" text: "t1647CustDeptName" isVariable: "true"
    Then PowerUnit - click - obj: "searchButton"
    Then PowerUnit - check-search-table-value - obj: "result_listTable" column: "Name" row: "1" value: "t1647CustDeptName" variable: "true"