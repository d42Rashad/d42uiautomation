Feature: Rack

  @t818 @smoke
  Scenario: [D42-T818] Verify the creation of a Rack
    Given Logged in as admin
    Then Rack - set-variable - key: "t818rackName" value: "t811" timestamp: "true" random: "true"
    Then Banner - hover - obj: "dataCenterLink"
    Then Banner - click - obj: "racksLink"
    Then Rack - click - obj: "addRackButton"
    Then Rack - validate-text - obj: "pageHeader" value: "Add Rack"
    Then Rack - validate-title - text: "Add Rack | Device42"
    Then Rack - type - obj: "nameTextbox" text: "t818rackName" isVariable: "true"
    Then Rack - type - obj: "roomTextbox" text: "5" isVariable: "false"
    Then Banner - click - obj: "saveButton"
    Then Rack - type - obj: "searchTextbox" text: "t818rackName" isVariable: "true"
    Then Rack - click - obj: "searchButton"
    Then Rack - check-search-table-value - obj: "result_listTable" column: "Name" row: "1" value: "t818rackName" variable: "true"

  @t823
  Scenario: [D42-T823] Verify rack list page - choose a couple filters
    Given Logged in as admin
    Then Banner - hover - obj: "dataCenterLink"
    Then Banner - click - obj: "racksLink"
    Then Rack - select - obj: "filterRoomDropdown" value: "OXDC1 @ OXP"
    Then Rack - select - obj: "filterTagsDropdown" value: "TestTag"
    Then Rack - check-search-table-value - obj: "result_listTable" column: "Name" row: "1" value: "RA118" variable: "false"
    Then Rack - validate-table-count - obj: "result_listTable" value: "1"

  @t824
  Scenario: [D42-T824] Verify user is able to move a rack from one room to another
    Given Logged in as admin
    Then Banner - hover - obj: "dataCenterLink"
    Then Banner - click - obj: "racksLink"
    Then Rack - type - obj: "searchTextbox" text: "NHCTDC1R1" isVariable: "false"
    Then Rack - click - obj: "searchButton"
    Then Rack - check-search-table-value - obj: "result_listTable" column: "Name" row: "1" value: "NHCTDC1R1" variable: "false"
    Then Rack - click-search-table-value - obj: "result_listTable" column: "Name" row: "1"
    Then Rack - click - obj: "editRackButton"
    Then Rack - clear - obj: "roomTextbox"
    Then Rack - type - obj: "roomTextbox" text: "1" isVariable: "false"
    Then Rack - click - obj: "continueButton"
    Then Rack - validate-text-contains - obj: "pageHeader" value: "IDF2 @ NHC"
    Then Rack - clear - obj: "roomTextbox"
    Then Rack - type - obj: "roomTextbox" text: "5" isVariable: "false"
    Then Rack - click - obj: "continueButton"
    Then Rack - validate-text-contains - obj: "pageHeader" value: "NHDC1 @ NHC"
    Then Banner - click - obj: "saveButton"

  @t832 @adjusts-existing-data
  Scenario: [D42-T832] Verify that user is able to see rack layout by hovering over the name
    Given Logged in as admin
    Then Banner - hover - obj: "dataCenterLink"
    Then Banner - click - obj: "racksLink"
    Then Rack - type - obj: "searchTextbox" text: "NHCTDC1R1" isVariable: "false"
    Then Rack - click - obj: "searchButton"
    Then Rack - check-search-table-value - obj: "result_listTable" column: "Name" row: "1" value: "NHCTDC1R1" variable: "false"
    Then Rack - click-search-table-value - obj: "result_listTable" column: "Name" row: "1"
    Then Rack - click-field-row-text - obj: "roomFieldRows" column: "Room" value: "NHDC1 @ NHC"
    Then Room - click - obj: "optionsButton"
    Then Room - click-options - obj: "dropDownToolsList" value: "Room Layout"
    Then Room - gridLayout-actions - action: "hover" value: "NHCTDC1R4"
    Then Room - validate-room-layout-property - property: "Name" text: "NHCTDC1R4"

  @t833
  Scenario: [D42-T833] Verify that rack contains a core device
    Given Logged in as admin
    Then Banner - hover - obj: "dataCenterLink"
    Then Banner - click - obj: "racksLink"
    Then Rack - type - obj: "searchTextbox" text: "NHCTDC1R4" isVariable: "false"
    Then Rack - click - obj: "searchButton"
    Then Rack - check-search-table-value - obj: "result_listTable" column: "Name" row: "1" value: "NHCTDC1R4" variable: "false"
    Then Rack - click-search-table-value - obj: "result_listTable" column: "Name" row: "1"
    Then Rack - validate-table-column-value - obj: "devicesInRackTable" column: "field-device" value: "NHCTCORE01"

  @t836 @smoke
  Scenario: [D42-T836] Verify the user is able to filter the devices BY ROW
    Given Logged in as admin
    Then Banner - hover - obj: "dataCenterLink"
    Then Banner - click - obj: "racksLink"
    Then Rack - select - obj: "filterRowDropdown" value: "1"
    Then Rack - validate-all-table-column-values - obj: "result_listTable" column: "Row" value: "1"

  @t837 @smoke
  Scenario: [D42-T837] Verify that user is able to add test rack
    Given Logged in as admin
    Then Rack - set-variable - key: "t837rackName" value: "t837" timestamp: "true" random: "true"
    Then Banner - hover - obj: "dataCenterLink"
    Then Banner - click - obj: "racksLink"
    Then Rack - click - obj: "addRackButton"
    Then Rack - type - obj: "nameTextbox" text: "t837rackName" isVariable: "true"
    Then Banner - click - obj: "saveButton"
    Then Rack - type - obj: "searchTextbox" text: "t837rackName" isVariable: "true"
    Then Rack - click - obj: "searchButton"
    Then Rack - check-search-table-value - obj: "result_listTable" column: "Name" row: "1" value: "t837rackName" variable: "true"

  @t839
  Scenario: [D42-T839] Verify that user is able to show front layout of the rack by performing action through action dropdown
    Given Logged in as admin
    Then Banner - hover - obj: "dataCenterLink"
    Then Banner - click - obj: "racksLink"
    Then Rack - type - obj: "searchTextbox" text: "NHCTDC1R" isVariable: "false"
    Then Rack - click - obj: "searchButton"
    Then Rack - click-results-table-value-by-key - obj: "result_listTable" searchColumn: "Name" clickColumn: "Checkbox" value: "NHCTDC1R1"
    Then Rack - click-results-table-value-by-key - obj: "result_listTable" searchColumn: "Name" clickColumn: "Checkbox" value: "NHCTDC1R4"
    Then Rack - select - obj: "actionDropdown" value: "Show Front Layout of selected racks"
    Then Rack - click - obj: "runSelectedActionButton"
    Then Rack - isDisplayed - obj: "drawingRack"

  @t840 @incomplete # This does not test rack device order when rearranging.
  Scenario: [D42-T840] Verify that user is able to rearrange the device and racks through drag and drop
    Given Logged in as admin
    Then Banner - hover - obj: "dataCenterLink"
    Then Banner - click - obj: "racksLink"
    Then Rack - type - obj: "searchTextbox" text: "NHCTDC1R1" isVariable: "false"
    Then Rack - click - obj: "searchButton"
    Then Rack - click-results-table-value-by-key - obj: "result_listTable" searchColumn: "Name" clickColumn: "Checkbox" value: "NHCTDC1R1"
    Then Rack - select - obj: "actionDropdown" value: "Show Front Layout of selected racks"
    Then Rack - click - obj: "runSelectedActionButton"
    Then Rack - click - obj: "editModeButton"
    Then Rack - hover-rack-device - obj: "USNHCS-DCM10"
    Then Rack - check-property-rack-popup-field - obj: "Name" value: "USNHCS-DCM10"
    Then Rack - drag-rack-selection - from: "USNHCTVH002" to: "6" index: "1"
    Then Rack - drag-rack-selection - from: "USNHCTVH002" to: "36" index: "1"

  @t841
  Scenario: [D42-T841] Verify that user is able open front view on multiple racks through action dropdown
    Given Logged in as admin
    Then Rack - set-variable - key: "t841rackName" value: "t841" timestamp: "true" random: "true"
    Then Banner - hover - obj: "dataCenterLink"
    Then Banner - click - obj: "racksLink"
    Then Rack - type - obj: "searchTextbox" text: "NHCTDC1R" isVariable: "false"
    Then Rack - click - obj: "searchButton"
    Then Rack - click-results-table-value-by-key - obj: "result_listTable" searchColumn: "Name" clickColumn: "Checkbox" value: "NHCTDC1R1"
    Then Rack - click-results-table-value-by-key - obj: "result_listTable" searchColumn: "Name" clickColumn: "Checkbox" value: "NHCTDC1R4"
    Then Rack - select - obj: "actionDropdown" value: "Show Front Layout of selected racks"
    Then Rack - click - obj: "runSelectedActionButton"
    Then Rack - isDisplayed - obj: "drawingRack"