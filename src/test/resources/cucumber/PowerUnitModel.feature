Feature: Power Unit Model

  @t851 @smoke
  Scenario: [D42-T851] Verify the creation of a new Power Unit Models of PDU type
    Given Logged in as admin
    Then PowerUnit - set-variable - key: "t843pduModelName" value: "t851" timestamp: "true" random: "true"
    Then Banner - navigation-left-menu - path: "DataCenter > Power Units > Power Unit Models"
    Then PowerUnitModel - click - obj: "addPowerUnitModelButton"
    Then PowerUnitModel - validate-text - obj: "pageHeader" value: "Add Power Unit Model"
    Then PowerUnitModel - validate-title - text: "Add Power Unit Model | Device42"
    Then PowerUnitModel - type - obj: "nameTextbox" text: "t843pduModelName" isVariable: "true"
    Then PowerUnitModel - click - obj: "saveButton"
    Then PowerUnitModel - type - obj: "searchTextbox" text: "t843pduModelName" isVariable: "true"
    Then PowerUnitModel - click - obj: "searchButton"
    Then PowerUnitModel - check-search-table-value - obj: "result_listTable" column: "Name" row: "1" value: "t843pduModelName" variable: "true"