@vServer
Feature: VServer

  @25kPerformanceUpdate #TODO: 1. Use left/right for set-variable-from-object-text when referencing the left or right table on VServer page instead of index.
  Scenario Outline: Execute Performance Update
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: <jobName> isVariable: "false"
    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "Job Name" row: "1"
    Then VServer - toggle - obj: "jobStatusArea" value: "Show"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Total attempted" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Port check failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Auth failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Discovery Exception" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Success" index: "2"
    Then VServer - persist-doql - view: "view_jobscore_v1_performance" key: <jobKey> jobName: <jobName> rc_name: <rc_name> ip_address: <ip_address>
    Examples:
      | jobName | jobKey | rc_name | ip_address |
      | "87Win_Part1" | "vServer - 109" | "rc1 with WDS" | "10.89.240.200" |
      | "87Nix_Part1" | "vServer - 110" | "rc1 with WDS" | "10.89.240.200" |
      | "89Win_Part1" | "vServer - 113" | "rc1 with WDS" | "10.89.240.200" |
      | "89Nix_Part1" | "vServer - 114" | "rc1 with WDS" | "10.89.240.200" |
      | "87Win_Part2" | "vServer - 95" | "rc2" | "10.89.240.200" |
      | "87Nix_Part2" | "vServer - 86" | "rc2" | "10.89.240.200" |
      | "89Win_Part2" | "vServer - 115" | "rc2" | "10.89.240.200" |
      | "89Nix_Part2" | "vServer - 116" | "rc2" | "10.89.240.200" |
      | "88Win_Part1" | "vServer - 111" | "rc3 with WDS" | "10.89.240.200" |
      | "88Nix_Part1" | "vServer - 112" | "rc3 with WDS" | "10.89.240.200" |
      | "89Win_Part3" | "vServer - 117" | "rc3 with WDS" | "10.89.240.200" |
      | "89Nix_Part3" | "vServer - 118" | "rc3 with WDS" | "10.89.240.200" |
      | "88Win_Part2" | "vServer - 96" | "rc4 with WDS" | "10.89.240.200" |
      | "88Nix_Part2" | "vServer - 87" | "rc4 with WDS" | "10.89.240.200" |
      | "89Win_Part4" | "vServer - 119" | "rc4 with WDS" | "10.89.240.200" |
      | "89Nix_Part4" | "vServer - 120" | "rc4 with WDS" | "10.89.240.200" |
      | "89Win_Part5" | "vServer - 94" | "rc5 with WDS" | "10.89.240.200" |
      | "89Nix_Part5" | "vServer - 88" | "rc5 with WDS" | "10.89.240.200" |

  @SanityPerformanceUpdate #TODO: 1. Use left/right for set-variable-from-object-text when referencing the left or right table on VServer page instead of index.
  Scenario Outline: Sanity Performance Update
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: <jobName> isVariable: "false"
    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "Job Name" row: "1"
    Then VServer - toggle - obj: "jobStatusArea" value: "Show"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Total attempted" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Port check failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Auth failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Discovery Exception" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Success" index: "2"
    Then VServer - persist-doql - view: "view_jobscore_v1_performance" key: <jobKey> jobName: <jobName>
    Examples:
      | jobName | jobKey |
      | "88Win_Part1" | "vServer - 55" |
      | "88Nix_Part2" | "vServer - 42" |

  @87Win_Part1 #TODO: 1. Use left/right for set-variable-from-object-text when referencing the left or right table on VServer page instead of index.
  Scenario: Execute 87Win_Part1
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: "87Win_Part1" isVariable: "false"
    Then VServer - click - obj: "searchButton"
#    Then VServer - click-search-table-value - obj: "result_listTable" column: "commands" row: "1"
#    Then VServer - wait-for-job-complete - view: "view_jobscore_v1_performance" key: "vServer - 109" jobName: "87Win_Part1" timeout "60" retries: "3000"
#    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "Job Name" row: "1"
    Then VServer - toggle - obj: "jobStatusArea" value: "Show"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Total attempted" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Port check failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Auth failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Discovery Exception" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Success" index: "2"
    Then VServer - persist-doql - view: "view_jobscore_v1_performance" key: "vServer - 109" jobName: "87Win_Part1"

  @87Win_Part2 #TODO: 1. Use left/right for set-variable-from-object-text when referencing the left or right table on VServer page instead of index.
  Scenario: Execute 87Win_Part2
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: "87Win_Part2" isVariable: "false"
    Then VServer - click - obj: "searchButton"
#    Then VServer - click-search-table-value - obj: "result_listTable" column: "commands" row: "1"
#    Then VServer - wait-for-job-complete - view: "view_jobscore_v1_performance" key: "vServer - 95" jobName: "87Win_Part2" timeout "60" retries: "3000"
#    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "Job Name" row: "1"
    Then VServer - toggle - obj: "jobStatusArea" value: "Show"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Total attempted" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Port check failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Auth failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Discovery Exception" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Success" index: "2"
    Then VServer - persist-doql - view: "view_jobscore_v1_performance" key: "vServer - 95" jobName: "87Win_Part2"

  @88Win_Part1 #TODO: 1. Use left/right for set-variable-from-object-text when referencing the left or right table on VServer page instead of index.
  Scenario: Execute 88Win_Part1
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: "88Win_Part1" isVariable: "false"
    Then VServer - click - obj: "searchButton"
#    Then VServer - click-search-table-value - obj: "result_listTable" column: "commands" row: "1"
#    Then VServer - wait-for-job-complete - view: "view_jobscore_v1_performance" key: "vServer - 111" jobName: "88Win_Part1" timeout "60" retries: "3000"
#    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "Job Name" row: "1"
    Then VServer - toggle - obj: "jobStatusArea" value: "Show"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Total attempted" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Port check failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Auth failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Discovery Exception" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Success" index: "2"
    Then VServer - persist-doql - view: "view_jobscore_v1_performance" key: "vServer - 111" jobName: "88Win_Part1"

  @88Win_Part2 #TODO: 1. Use left/right for set-variable-from-object-text when referencing the left or right table on VServer page instead of index.
  Scenario: Execute 88Win_Part2
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: "88Win_Part2" isVariable: "false"
    Then VServer - click - obj: "searchButton"
#    Then VServer - click-search-table-value - obj: "result_listTable" column: "commands" row: "1"
#    Then VServer - wait-for-job-complete - view: "view_jobscore_v1_performance" key: "vServer - 96" jobName: "88Win_Part2" timeout "60" retries: "3000"
#    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "Job Name" row: "1"
    Then VServer - toggle - obj: "jobStatusArea" value: "Show"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Total attempted" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Port check failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Auth failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Discovery Exception" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Success" index: "2"
    Then VServer - persist-doql - view: "view_jobscore_v1_performance" key: "vServer - 96" jobName: "88Win_Part2"

  @89Win_Part1 #TODO: 1. Use left/right for set-variable-from-object-text when referencing the left or right table on VServer page instead of index.
  Scenario: Execute 89Win_Part1
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: "89Win_Part1" isVariable: "false"
    Then VServer - click - obj: "searchButton"
#    Then VServer - click-search-table-value - obj: "result_listTable" column: "commands" row: "1"
#    Then VServer - wait-for-job-complete - view: "view_jobscore_v1_performance" key: "vServer - 113" jobName: "89Win_Part1" timeout "60" retries: "3000"
#    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "Job Name" row: "1"
    Then VServer - toggle - obj: "jobStatusArea" value: "Show"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Total attempted" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Port check failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Auth failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Discovery Exception" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Success" index: "2"
    Then VServer - persist-doql - view: "view_jobscore_v1_performance" key: "vServer - 113" jobName: "89Win_Part1"

  @89Win_Part2 #TODO: 1. Use left/right for set-variable-from-object-text when referencing the left or right table on VServer page instead of index.
  Scenario: Execute 89Win_Part2
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: "89Win_Part2" isVariable: "false"
    Then VServer - click - obj: "searchButton"
#    Then VServer - click-search-table-value - obj: "result_listTable" column: "commands" row: "1"
#    Then VServer - wait-for-job-complete - view: "view_jobscore_v1_performance" key: "vServer - 115" jobName: "89Win_Part2" timeout "60" retries: "3000"
#    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "Job Name" row: "1"
    Then VServer - toggle - obj: "jobStatusArea" value: "Show"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Total attempted" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Port check failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Auth failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Discovery Exception" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Success" index: "2"
    Then VServer - persist-doql - view: "view_jobscore_v1_performance" key: "vServer - 115" jobName: "89Win_Part2"

  @89Win_Part3 #TODO: 1. Use left/right for set-variable-from-object-text when referencing the left or right table on VServer page instead of index.
  Scenario: Execute 89Win_Part3
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: "89Win_Part3" isVariable: "false"
    Then VServer - click - obj: "searchButton"
#    Then VServer - click-search-table-value - obj: "result_listTable" column: "commands" row: "1"
#    Then VServer - wait-for-job-complete - view: "view_jobscore_v1_performance" key: "vServer - 117" jobName: "89Win_Part3" timeout "60" retries: "3000"
#    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "Job Name" row: "1"
    Then VServer - toggle - obj: "jobStatusArea" value: "Show"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Total attempted" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Port check failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Auth failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Discovery Exception" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Success" index: "2"
    Then VServer - persist-doql - view: "view_jobscore_v1_performance" key: "vServer - 117" jobName: "89Win_Part3"

  @89Win_Part4 #TODO: 1. Use left/right for set-variable-from-object-text when referencing the left or right table on VServer page instead of index.
  Scenario: Execute 89Win_Part4
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: "89Win_Part4" isVariable: "false"
    Then VServer - click - obj: "searchButton"
#    Then VServer - click-search-table-value - obj: "result_listTable" column: "commands" row: "1"
#    Then VServer - wait-for-job-complete - view: "view_jobscore_v1_performance" key: "vServer - 119" jobName: "89Win_Part4" timeout "60" retries: "3000"
#    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "Job Name" row: "1"
    Then VServer - toggle - obj: "jobStatusArea" value: "Show"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Total attempted" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Port check failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Auth failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Discovery Exception" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Success" index: "2"
    Then VServer - persist-doql - view: "view_jobscore_v1_performance" key: "vServer - 119" jobName: "89Win_Part4"

  @89Win_Part5 #TODO: 1. Use left/right for set-variable-from-object-text when referencing the left or right table on VServer page instead of index.
  Scenario: Execute 89Win_Part5
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: "89Win_Part5" isVariable: "false"
    Then VServer - click - obj: "searchButton"
#    Then VServer - click-search-table-value - obj: "result_listTable" column: "commands" row: "1"
#    Then VServer - wait-for-job-complete - view: "view_jobscore_v1_performance" key: "vServer - 94" jobName: "89Win_Part5" timeout "60" retries: "3000"
#    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "Job Name" row: "1"
    Then VServer - toggle - obj: "jobStatusArea" value: "Show"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Total attempted" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Port check failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Auth failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Discovery Exception" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Success" index: "2"
    Then VServer - persist-doql - view: "view_jobscore_v1_performance" key: "vServer - 94" jobName: "89Win_Part5"

  @87Nix_Part1 #TODO: 1. Use left/right for set-variable-from-object-text when referencing the left or right table on VServer page instead of index.
  Scenario: Execute 87Nix_Part1
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: "87Nix_Part1" isVariable: "false"
    Then VServer - click - obj: "searchButton"
#    Then VServer - click-search-table-value - obj: "result_listTable" column: "commands" row: "1"
#    Then VServer - wait-for-job-complete - view: "view_jobscore_v1_performance" key: "vServer - 110" jobName: "87Nix_Part1" timeout "60" retries: "3000"
#    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "Job Name" row: "1"
    Then VServer - toggle - obj: "jobStatusArea" value: "Show"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Total attempted" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Port check failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Auth failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Discovery Exception" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Success" index: "2"
    Then VServer - persist-doql - view: "view_jobscore_v1_performance" key: "vServer - 110" jobName: "87Nix_Part1"

  @87Nix_Part2 #TODO: 1. Use left/right for set-variable-from-object-text when referencing the left or right table on VServer page instead of index.
  Scenario: Execute 87Nix_Part2
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: "87Nix_Part2" isVariable: "false"
    Then VServer - click - obj: "searchButton"
#    Then VServer - click-search-table-value - obj: "result_listTable" column: "commands" row: "1"
#    Then VServer - wait-for-job-complete - view: "view_jobscore_v1_performance" key: "vServer - 86" jobName: "87Nix_Part2" timeout "60" retries: "3000"
#    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "Job Name" row: "1"
    Then VServer - toggle - obj: "jobStatusArea" value: "Show"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Total attempted" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Port check failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Auth failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Discovery Exception" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Success" index: "2"
    Then VServer - persist-doql - view: "view_jobscore_v1_performance" key: "vServer - 86" jobName: "87Nix_Part2"

  @88Nix_Part1 #TODO: 1. Use left/right for set-variable-from-object-text when referencing the left or right table on VServer page instead of index.
  Scenario: Execute 88Nix_Part1
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: "88Nix_Part1" isVariable: "false"
    Then VServer - click - obj: "searchButton"
#    Then VServer - click-search-table-value - obj: "result_listTable" column: "commands" row: "1"
#    Then VServer - wait-for-job-complete - view: "view_jobscore_v1_performance" key: "vServer - 95" jobName: "88Nix_Part1" timeout "60" retries: "3000"
#    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "Job Name" row: "1"
    Then VServer - toggle - obj: "jobStatusArea" value: "Show"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Total attempted" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Port check failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Auth failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Discovery Exception" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Success" index: "2"
    Then VServer - persist-doql - view: "view_jobscore_v1_performance" key: "vServer - 95" jobName: "88Nix_Part1"

  @88Nix_Part2 #TODO: 1. Use left/right for set-variable-from-object-text when referencing the left or right table on VServer page instead of index.
  Scenario: Execute 88Nix_Part2
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: "88Nix_Part2" isVariable: "false"
    Then VServer - click - obj: "searchButton"
#    Then VServer - click-search-table-value - obj: "result_listTable" column: "commands" row: "1"
#    Then VServer - wait-for-job-complete - view: "view_jobscore_v1_performance" key: "vServer - 87" jobName: "88Nix_Part2" timeout "60" retries: "3000"
#    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "Job Name" row: "1"
    Then VServer - toggle - obj: "jobStatusArea" value: "Show"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Total attempted" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Port check failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Auth failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Discovery Exception" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Success" index: "2"
    Then VServer - persist-doql - view: "view_jobscore_v1_performance" key: "vServer - 87" jobName: "88Nix_Part2"

  @89Nix_Part1 #TODO: 1. Use left/right for set-variable-from-object-text when referencing the left or right table on VServer page instead of index.
  Scenario: Execute 89Nix_Part1
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: "89Nix_Part1" isVariable: "false"
    Then VServer - click - obj: "searchButton"
#    Then VServer - click-search-table-value - obj: "result_listTable" column: "commands" row: "1"
#    Then VServer - wait-for-job-complete - view: "view_jobscore_v1_performance" key: "vServer - 119" jobName: "89Nix_Part1" timeout "60" retries: "3000"
#    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "Job Name" row: "1"
    Then VServer - toggle - obj: "jobStatusArea" value: "Show"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Total attempted" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Port check failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Auth failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Discovery Exception" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Success" index: "2"
    Then VServer - persist-doql - view: "view_jobscore_v1_performance" key: "vServer - 119" jobName: "89Nix_Part1"

  @89Nix_Part2 #TODO: 1. Use left/right for set-variable-from-object-text when referencing the left or right table on VServer page instead of index.
  Scenario: Execute 89Nix_Part2
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: "89Nix_Part2" isVariable: "false"
    Then VServer - click - obj: "searchButton"
#    Then VServer - click-search-table-value - obj: "result_listTable" column: "commands" row: "1"
#    Then VServer - wait-for-job-complete - view: "view_jobscore_v1_performance" key: "vServer - 114" jobName: "89Nix_Part2" timeout "60" retries: "3000"
#    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "Job Name" row: "1"
    Then VServer - toggle - obj: "jobStatusArea" value: "Show"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Total attempted" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Port check failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Auth failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Discovery Exception" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Success" index: "2"
    Then VServer - persist-doql - view: "view_jobscore_v1_performance" key: "vServer - 114" jobName: "89Nix_Part2"

  @89Nix_Part3 #TODO: 1. Use left/right for set-variable-from-object-text when referencing the left or right table on VServer page instead of index.
  Scenario: Execute 89Nix_Part3
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: "89Nix_Part3" isVariable: "false"
    Then VServer - click - obj: "searchButton"
#    Then VServer - click-search-table-value - obj: "result_listTable" column: "commands" row: "1"
#    Then VServer - wait-for-job-complete - view: "view_jobscore_v1_performance" key: "vServer - 118" jobName: "89Nix_Part3" timeout "60" retries: "3000"
#    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "Job Name" row: "1"
    Then VServer - toggle - obj: "jobStatusArea" value: "Show"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Total attempted" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Port check failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Auth failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Discovery Exception" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Success" index: "2"
    Then VServer - persist-doql - view: "view_jobscore_v1_performance" key: "vServer - 118" jobName: "89Nix_Part3"

  @89Nix_Part4 #TODO: 1. Use left/right for set-variable-from-object-text when referencing the left or right table o    n VServer page instead of index.
  Scenario: Execute 89Nix_Part4
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: "89Nix_Part4" isVariable: "false"
    Then VServer - click - obj: "searchButton"
#    Then VServer - click-search-table-value - obj: "result_listTable" column: "commands" row: "1"
#    Then VServer - wait-for-job-complete - view: "view_jobscore_v1_performance" key: "vServer - 120" jobName: "89Nix_Part4" timeout "60" retries: "3000"
#    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "Job Name" row: "1"
    Then VServer - toggle - obj: "jobStatusArea" value: "Show"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Total attempted" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Port check failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Auth failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Discovery Exception" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Success" index: "2"
    Then VServer - persist-doql - view: "view_jobscore_v1_performance" key: "vServer - 120" jobName: "89Nix_Part4"

  @89Nix_Part5 #TODO: 1. Use left/right for set-variable-from-object-text when referencing the left or right table on VServer page instead of index.
  Scenario: Execute 89Nix_Part5
    Given Logged in as admin
    Then Banner - navigation-left-menu - path: "Discovery > HyperVisors / *nix / Windows"
    Then VServer - type - obj: "searchTextbox" text: "89Nix_Part5" isVariable: "false"
    Then VServer - click - obj: "searchButton"
#    Then VServer - click-search-table-value - obj: "result_listTable" column: "commands" row: "1"
#    Then VServer - wait-for-job-complete - view: "view_jobscore_v1_performance" key: "vServer - 88" jobName: "89Nix_Part5" timeout "60" retries: "3000"
#    Then VServer - click - obj: "searchButton"
    Then VServer - click-search-table-value - obj: "result_listTable" column: "Job Name" row: "1"
    Then VServer - toggle - obj: "jobStatusArea" value: "Show"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Total attempted" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Port check failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Auth failed" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Discovery Exception" index: "2"
    Then VServer - set-variable-from-object-text - obj: "jobStatusContainer" value: "Success" index: "2"
    Then VServer - persist-doql - view: "view_jobscore_v1_performance" key: "vServer - 88" jobName: "89Nix_Part5"

