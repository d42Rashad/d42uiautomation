Feature: Secret

  @t773 @smoke
  Scenario: [D42-T773] Verify burnt passwords can't be seen
    Given Logged in as admin
    Then Secret - set-variable - key: "t773SecretName" value: "UISecret" timestamp: "true" random: "false"
    Then Banner - click - obj: "secretsLink"
    Then Secret - click - obj: "addPasswordButton"
    Then Secret - type - obj: "usernameTextbox" text: "t773SecretName" isVariable: "true"
    Then Secret - type - obj: "labelTextbox" text: "testLabel" isVariable: "false"
    Then Secret - type - obj: "daysToExpireTextbox" text: "2" isVariable: "false"
    Then Secret - type - obj: "passwordTextarea" text: "testPassword" isVariable: "false"
    Then Secret - click - obj: "secretTextPeekToggle"
    Then Secret - check-attribute - obj: "passwordTextarea" attribute: "value" value: "testPassword"
    Then Secret - check-attribute - obj: "passwordContainer" attribute: "class" value: "secret-text-container"
    Then Secret - click - obj: "secretTextPeekToggle"
    Then Secret - check-attribute - obj: "passwordTextarea" attribute: "value" value: "testPassword"
    Then Secret - check-attribute - obj: "passwordContainer" attribute: "class" value: "secret-text-container secret-text-enabled"
    Then Secret - select - obj: "storageDropdown" value: "Burnt"
    Then Banner - click - obj: "saveButton"
    Then Secret - type - obj: "searchTextbox" text: "t773SecretName" isVariable: "true"
    Then Secret - click - obj: "searchButton"
    Then Secret - check-search-table-value - obj: "result_listTable" column: "Username" row: "1" value: "t773SecretName" variable: "true"
    Then Secret - check-search-table-value - obj: "result_listTable" column: "Password Storage" row: "1" value: "Burnt" variable: "false"
    Then Secret - click-search-table-value - obj: "result_listTable" column: "Username" row: "1"
    Then Secret - click - obj: "editSecretButton"
    Then Secret - check-attribute - obj: "passwordTextarea" attribute: "placeholder" value: "Password cannot be viewed.  Set value to reset password or leave empty to use existing value"
