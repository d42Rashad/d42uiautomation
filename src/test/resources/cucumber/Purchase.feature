Feature: Purchases

  @1651 @smoke
  Scenario: [D42-T1651] Verify the creation of a new Purchase
    Given Logged in as admin
    Then Purchase - set-variable - key: "t1651purchaseName" value: "t1651" timestamp: "true" random: "true"
    Then Banner - navigation-left-menu - path: "DataCenter > Purchases"
    Then Purchase - click - obj: "addPurchaseButton"
    Then Purchase - validate-text - obj: "pageHeader" value: "Add Purchase"
    Then Purchase - validate-title - text: "Add Purchase | Device42"
    Then Purchase - type - obj: "orderNameNoTextbox" text: "t1651purchaseName" isVariable: "true"
    Then Purchase - click - obj: "saveButton"
    Then Purchase - type - obj: "searchTextbox" text: "t1651purchaseName" isVariable: "true"
    Then Purchase - click - obj: "searchButton"
    Then Purchase - check-search-table-value - obj: "result_listTable" column: "Order Name/#" row: "1" value: "t1651purchaseName" variable: "true"






