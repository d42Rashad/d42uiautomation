#!/bin/bash
#mvn clean test verify -Dmaven.test.failure.ignore=true
containername=debug3
docker run --name $containername --env browser=CHROME --env browser_version=79.0.3945.117 --env host=https://10.89.240.200 --env adaptavist_test_cycle_key=D42-52 --env adaptavist_comment_template=test\ comment --env threads=3 --env version=16.16.00-1598047108 d42/uiautomation:1.0.0 "clean" "test" "verify" "-Dmaven.test.failure.ignore=true" "-Dcucumber.filter.tags=@25kPerformanceUpdate"

