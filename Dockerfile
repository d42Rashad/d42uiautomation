FROM java:8-alpine

RUN apk add --update ca-certificates && rm -rf /var/cache/apk/* && \
  find /usr/share/ca-certificates/mozilla/ -name "*.crt" -exec keytool -import -trustcacerts \
  -keystore /usr/lib/jvm/java-1.8-openjdk/jre/lib/security/cacerts -storepass changeit -noprompt \
  -file {} -alias {} \; && \
  keytool -list -keystore /usr/lib/jvm/java-1.8-openjdk/jre/lib/security/cacerts --storepass changeit

ENV MAVEN_VERSION 3.5.4
ENV MAVEN_HOME /usr/lib/mvn
ENV PATH $MAVEN_HOME/bin:$PATH

RUN wget http://archive.apache.org/dist/maven/maven-3/$MAVEN_VERSION/binaries/apache-maven-$MAVEN_VERSION-bin.tar.gz && \
  tar -zxvf apache-maven-$MAVEN_VERSION-bin.tar.gz && \
  rm apache-maven-$MAVEN_VERSION-bin.tar.gz && \
  mv apache-maven-$MAVEN_VERSION /usr/lib/mvn

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY pom.xml ./

RUN mvn install

ENV platform LINUX
ENV browser CHROME
ENV version 16.11.00.1588211684
ENV browser_version 79.0.3945.117
#ENV browser_version 80.0.3987.106
ENV grid_ip 10.90.4.28
#ENV grid_ip 192.168.99.100
ENV grid_port 4444
ENV ping_timeout 100
ENV host https://10.89.240.200
ENV db_ip 10.90.4.28
ENV db_port 5432
ENV implicit_wait 60
ENV explicit_short_wait 3
ENV explicit_med_wait 5
ENV admin_username admin
ENV admin_password adm!nd42
ENV adaptavist_bearer_token eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjb250ZXh0Ijp7InV1aWQiOiJKZldBaFUtd1kiLCJhcGlHd0tleSI6IjlPcnU0bDA5UWo2QURwajJDbklIZzRDSEhobzZVY2k0M1I5UmpuMnoiLCJiYXNlVXJsIjoiaHR0cHM6Ly9kZXZpY2U0Mi5hdGxhc3NpYW4ubmV0IiwidXNlciI6eyJhY2NvdW50SWQiOiI1ZGQyYzRhNmI1OTMzZDBlZWZhZjIxZTkifX0sImlhdCI6MTU3ODQxMjcyMSwiZXhwIjoxNjA5OTcwMzIxLCJpc3MiOiJjb20ua2Fub2FoLnRlc3QtbWFuYWdlciIsInN1YiI6ImppcmE6YzAyNzE1MTctMWNkNS00ZmVmLTk3NTEtMzE4N2RmNjlkODVlIn0.WBcOl45YW2YFtJszQgRKaaucEdfNR6Vr6BywW-Rf_q8
ENV adaptavist_url https://api.adaptavist.io/tm4j/v2/testexecutions/
ENV adaptavist_project_key D42
ENV adaptavist_test_cycle_key D42-R52
ENV adaptavist_comment_template comment
ENV postToAdaptavist false
ENV threads 1

COPY . .

ENTRYPOINT [ "mvn" ]

CMD [ "clean", "test", "verify", "-Dmaven.test.failure.ignore=true", "-Dcucumber.filter.tags=@PerformanceUpdate" ]